window.onload = function(){
    $.ajax({
        method:"POST",
        url:"./accionMostrarAlmacenes.php",
        data:{}
    }).done(function(data){    
        //console.log(data)           
        var json = JSON.parse(data);                          
        //console.log(json)
        for(var i=0; i < json.length;i++){            
            console.log(json[i].id_producto[0].producto)
            $("#tableAlmacenes").append(
                "<tr>"+
                    "<th scope='row'>"+ json[i].id_almacen +"</th>"+
                    "<td>"+ json[i].id_producto[0].producto +"</td>"+
                    "<td>" + json[i].entrada + "</td>" + "<td>"+ json[i].salida +"</td>"+ "<td>" + json[i].fecha + "</td>" +
                    "<td>"+
                        //"<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_almacen+"' onclick='detalleAlmacen("+json[i].id_almacen+")'>Detalles</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_almacen+"' onclick='modificarAlmacen("+json[i].id_almacen+","+json[i].entrada+","+json[i].salida+")'>Modificar</button>&nbsp;"+
                        "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_almacen+"' onclick='eliminarAlmacen("+json[i].id_almacen+")'>Eliminar</button>"+
                    "</td>"+
                "</tr>"
                );                                        
        }
    })
    $.ajax({
        method:"POST",
        url:"./llenarComboProducto.php"
    }).done(function(data){                
        var json = JSON.parse(data);                     
        //console.log(json);
        for(var i=0; i < json.length;i++){            
            $("#selectProducto").append("<option value='" + json[i].id_producto + "'>" + json[i].producto + "</option>");                                   
        }               
    })
}
functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
    )    
}

functionSucces = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}