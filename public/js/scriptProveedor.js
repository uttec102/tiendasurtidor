window.onload = function(){     
    $.ajax({
        method:"POST",
        url:"./accionMostrarProveedores.php",
        data:{}
    }).done(function(data){               
        var json = JSON.parse(data);                     
        //console.log(json);        
        for(var i=0; i < json.length;i++){  
            //console.log(json[i].status)
            if(json[i].status==1){
                $("#tableProveedores").append(
                    "<tr>"+
                        "<th scope='row'>"+ json[i].id_proveedor +"</th>"+
                        "<td>"+ json[i].proveedor +"</td>"+ "<td>" + json[i].contacto + "</td>" +
                        "<td>Alta</td>"+
                        "<td>"+
                            "<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_proveedor+"' onclick='detalleProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Detalles</button>&nbsp;"+
                            "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value="+json[i].id_proveedor+" onclick='modificarProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`,`"+json[i].contacto+"`,"+json[i].telefono1+",`"+json[i].correo1+"`,"+json[i].telefono2+",`"+json[i].correo2+"`)'>Modificar</button>&nbsp;"+
                            "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_proveedor+"' onclick='eliminarProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Baja</button>"+
                        "</td>"+
                    "</tr>"
                );
            }else{
                $("#tableProveedores").append(
                    "<tr>"+
                        "<th scope='row'>"+ json[i].id_proveedor +"</th>"+
                        "<td>"+ json[i].proveedor +"</td>"+ "<td>" + json[i].contacto + "</td>" +
                        "<td>Baja</td>"+
                        "<td>"+
                            "<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_proveedor+"' onclick='detalleProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Detalles</button>&nbsp;"+
                            "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value="+json[i].id_proveedor+" onclick='modificarProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`,`"+json[i].contacto+"`,"+json[i].telefono1+",`"+json[i].correo1+"`,"+json[i].telefono2+",`"+json[i].correo2+"`)'>Modificar</button>&nbsp;"+
                            "<button class='btn btn-primary' value='"+json[i].id_proveedor+"' onclick='altaProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Alta</button>"+
                        "</td>"+
                    "</tr>"
                );
            }       
            /*$("#tableProveedores").append(
                "<tr>"+
                    "<th scope='row'>"+ json[i].id_proveedor +"</th>"+
                    "<td>"+ json[i].proveedor +"</td>"+ "<td>" + json[i].contacto + "</td>" +
                    "<td>sdfsdf</td>"+
                    "<td>"+
                        "<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_proveedor+"' onclick='detalleProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Detalles</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value="+json[i].id_proveedor+" onclick='modificarProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`,`"+json[i].contacto+"`,"+json[i].telefono1+",`"+json[i].correo1+"`,"+json[i].telefono2+",`"+json[i].correo2+"`)'>Modificar</button>&nbsp;"+
                        "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_proveedor+"' onclick='eliminarProveedor("+json[i].id_proveedor+",`"+json[i].proveedor+"`)'>Eliminar</button>"+
                    "</td>"+
                "</tr>"
            );*/                                        
        }
    });
    $.ajax({
        method:"POST",
        url:"./estados.php"
    }).done(function(data){                
        var json = JSON.parse(data);                     
        //console.log(json);
        for(var i=0; i < json.length;i++){            
            $("#selectEstado").append("<option value='" + json[i].id_estado + "'>" + json[i].estado + "</option>");                                   
        }               
    });

    $.ajax({
        method:"POST",
        url:"./municipios.php"
    }).done(function(data){                
        var json = JSON.parse(data);                     
        //console.log(json);
        for(var i=0; i < json.length;i++){            
            $("#selectMunicipio").append("<option value='" + json[i].id_municipio + "'>" + json[i].municipio + "</option>");                                   
        }               
    })

    $.ajax({
        method:"POST",
        url:"./colonias.php"
    }).done(function(data){                
        var json = JSON.parse(data);                     
        //console.log(json);
        for(var i=0; i < json.length;i++){            
            $("#selectColonia").append("<option value='" + json[i].id_colonia + "'>" + json[i].colonia + "</option>");                                   
        }               
    })
    $.ajax({
        method:"POST",
        url:"./codigos.php"
    }).done(function(data){                
        var json = JSON.parse(data);                     
        //console.log(json);
        for(var i=0; i < json.length;i++){            
            $("#selectCodigoPostal").append("<option value='" + json[i].id_codpos + "'>" + json[i].codpos + "</option>");                                   
        }               
    })
}
functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
    )    
}

functionSucces = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    console.log("vaciar")
    $("#proveedorDetalle").empty()
    $("#contactoDetalle").empty()
    $("#telefonoPDetalle").empty()
    $("#correoPDetalle").empty()
    $("#telefonoSDetalle").empty()
    $("#correoSDetalle").empty()    
    $("#estadoDetalle").empty()
    $("#municipioDetalle").empty()
    $("#coloniaDetalle").empty()
    $("#codigoPostalDetalle").empty()
    $("#calleDetalle").empty()
}