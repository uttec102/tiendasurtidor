registrarProveedor = function(){
    var nombreCompania = $("#proveedor").val();
    var nombreContacto = $("#contacto").val();
    var telefonoPrincipal = $("#telefono1").val();
    var correoPrincipal = $("#correo1").val();
    var telefonoSecundario = $("#telefono2").val();
    var correoSecundario = $("#correo2").val();
    var estado = $("#selectEstado").val();
    var municipio = $("#selectMunicipio").val();
    var colonia = $("#selectColonia").val();
    var codigoPostal = $("#selectCodigoPostal").val();
    var calle = $("#calle").val();
    var numeroExterior = $("#nExt").val();
    var numeroInterior = $("#nInt").val();
    var expresionRegular = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if(nombreCompania.length < 5){
        functionError("El nombre de la compañía debe ser por lo menos de 8 caracteres.");
    }else{
        if (nombreContacto.length < 10){
            functionError("El nombre del contacto debe ser por lo menos de 10 caracteres.");
        }else{
            if(telefonoPrincipal.length < 12 || telefonoPrincipal.length >12 || telefonoSecundario.length < 12 || telefonoSecundario.length >12){
                functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
            }else{
                 if(!expresionRegular.test(correoPrincipal) || !expresionRegular.test(correoSecundario)){
                    functionError("Formato inválido para el correo. Favor de revisar sus datos.");
                }else{
                    $.ajax({
                        method:"POST",
                        url:"./accionRegProveedor.php",
                        data:{
                            compania:nombreCompania,
                           contacto:nombreContacto,                        
                            telefonoP:telefonoPrincipal,
                            correoP:correoPrincipal,
                            telefonoS:telefonoSecundario,
                            correoS:correoSecundario,
                            estado:estado,
                            municipio:municipio,
                            colonia:colonia,
                            codigoP:codigoPostal,
                            calle:calle,
                            nExt:numeroExterior,
                            nInt:numeroInterior
                        }
                        /*processData: false,
                        contentType: "application/json"*/
                    }).done(function(data){      
                        if(data == 0){
                            functionSucces("Proveedor registrado con exito.");
                            vaciarCampos();
                            window.location = "proveedor.php";
                        }else if(data == 1){
                            functionError("Proveedor ya existente, verifique su información.")
                        }else if(data == 2){
                            functionError("Ocurrrió un error, intente más tarde")
                        }     
                    })
                }
            }
        }
    }
}


    detalleProveedor = function(id, proveedor){
        vaciarCamposModal();       
        $("#detalleProveedorMostrar").append(proveedor)
        $.ajax({
            method:"POST",
            url:"./accionDetalleProveedor.php",
            data:{id:id}
        }).done(function(data){              
            //console.log(data)
            var json = JSON.parse(data);
            //console.log(json[0])
            $("#proveedorDetalle").append(json[0].proveedor);
            $("#contactoDetalle").append(json[0].contacto);
            $("#telefonoPDetalle").append(json[0].telefono1);
            $("#correoPDetalle").append(json[0].correo1);
            $("#telefonoSDetalle").append(json[0].telefono2);
            $("#correoSDetalle").append(json[0].correo2);

            
            //-------------CHECAR EL ESTADO Y ESE PEDO EN LA MODAL-------------

            $("#estadoDetalle").append(json[0].id_estado[0].estado);
            $("#municipioDetalle").append(json[0].id_municipio[0].municipio);
            $("#coloniaDetalle").append(json[0].id_colonia[0].colonia); 
            $("#codigoPostalDetalle").append(json[0].id_codpos[0].codpos);
            $("#calleDetalle").append(json[0].calle);
            /*$("#nExtDetalle").append(json[0].Noext);
            $("#nIntDetalle").append(json[0].Noint);*/
            //IMPORTANTE $("#divImgDetalle").append("<center><img src='../images/"+json[0].imagen+"' width='80%'></center>");
        }) ;
    }

    eliminarProveedor = function(id, proveedor){
        $("#comprobarEliminarProveedor").empty();
        $("#comprobarEliminarProveedor").append(proveedor); 
        $("#eliminarProveedor").val(id); 
    }

    eliminarProveedorDefinitivo = function(){
        var id_eliminar = $("#eliminarProveedor").val();
        console.log(id_eliminar);
        $.ajax({
            method:"POST",
            url:"./accionEliminarProveedor.php",
            data:{
                id:id_eliminar
            }
        }).done(function(data){        
            //console.log(data)
            if(data == 0){       
                functionSucces("Proveedor eliminado correctamente.");     
                window.location = "proveedor.php";
            }else{
                functionError("Ocurrió un error");
            }
        })   
    }
    modificarProveedor = function(id, proveedor, contacto, telefono1, correo1, telefono2, correo2){
        $("#proveedorModifica").empty();
        $("#proveedorModifica").append(proveedor);
        $("#nuevoContacto").val(""+contacto+"");
        $("#nuevoTelefonoP").val(telefono1);
        $("#nuevoCorreoP").val(""+correo1+"");
        $("#nuevoTelefonoS").val(telefono2);
        $("#nuevoCorreoS").val(""+correo2+"");
        $("#idProveedorModificar").val(id);
    }
    
    modificarProveedorDefinitiva = function(){    
        var nContacto = $("#nuevoContacto").val();
        var nTelefonoP = $("#nuevoTelefonoP").val();
        var nCorreoP = $("#nuevoCorreoP").val();
        var nTelefonoS = $("#nuevoTelefonoS").val();
        var nCorreoS = $("#nuevoCorreoS").val();
        if(nContacto.length == 0 || nTelefonoP.length == 0 || nCorreoP.length == 0 || nTelefonoS.length == 0 || nCorreoS.length == 0){
            functionError("No puede haber datos vacíos.");
        }else{
            if (nContacto.length < 10){
                functionError("El nombre del contacto debe ser por lo menos de 10 caracteres.");
            }else{
                if(nTelefonoP.length < 12 || nTelefonoP.length >12){
                    functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
                }else{
                     if(nTelefonoS.length < 12 || nTelefonoS.length >12){
                        functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
                    }else{
                        $.ajax({
                            method:"POST",
                            url:"./accionModProveedor.php",
                            data:{
                                contacto:nContacto,
                                telefonoP:nTelefonoP,
                                correoP:nCorreoP,
                                telefonoS:nTelefonoS,
                                correoS:nCorreoS,
                                idModificar:$("#idProveedorModificar").val()
                            }
                        }).done(function(data){        
                            if(data == 0){                  
                                functionSuccess();              
                                window.location = "proveedor.php";
                            }else if(data == 1){
                                functionError("Ocurrrió un error, intente más tarde")
                            }
                        }) 
                    }      
                }
            }
        }
    }
functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
    )    
}

functionSuccess = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    $("#proveedor").val("");
    $("#contacto").val("");
    $("#telefono1").val("");
    $("#correo1").val("");
    $("#telefono2").val("");
    $("#correo2").val("");
    $("#selectEstado").val("");
    $("#selectMunicipio").val("");
    $("#selectColonia").val("");
    $("#selectCodigoPostal").val("");
    $("#calle").val("");
    $("#nExt").val("");
    $("#nInt").val("");
}
vaciarCamposModal = function(){
    $("#detalleProveedorMostrar").empty();
    $("#proveedorDetalle").empty();
    $("#contactoDetalle").empty();
    $("#telefonoPDetalle").empty();
    $("#correoPDetalle").empty();
    $("#telefonoSDetalle").empty();
    $("#correoSDetalle").empty();
    $("#estadoDetalle").empty();
    $("#municipioDetalle").empty();
    $("#coloniaDetalle").empty();
    $("#codigoPostalDetalle").empty();
    $("#calleDetalle").empty();
    $("#nExtDetalle").empty();
    $("#nIntDetalle").empty();
}

altaProveedor = function(id, proveedor){
    console.log(id + proveedor)
    $.ajax({
        method:"POST",
        url:"./accionAltaProveedor.php",
        data:{
            id:id,
        },
        beforeSend: function(){
            functionWait();
        }
    }).done(function(data){               
        console.log(data)
        if(data==0){
            functionSuccess("Actualización exitosa")
            window.location = "proveedor.php";
            vaciarCampos()                
        }else if(data==1){
            functionError("Intente más tarde")
        }
    })
}