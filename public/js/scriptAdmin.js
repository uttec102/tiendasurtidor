window.onload = function(){
    $.ajax({
        method:"POST",
        url:"./accionMostrarCategoria.php",
        data:{}
    }).done(function(data){        
        //console.log(data)        
        var json = JSON.parse(data);                     
        //console.log(json);        
        for(var i=0; i < json.length;i++){            
            $("#categoriaProducto").append(
                "<option value='"+json[i].id_categoria+"'>"+json[i].categoria+"</option>"
                );                                   
        }
    }) 

    $.ajax({
        method:"POST",
        url:"./accionMostrarProductos.php",
        data:{}
    }).done(function(data){        
        //console.log(data)        
        var json = JSON.parse(data);                     
        //console.log(json);        
        for(var i=0; i < json.length;i++){            
            $("#tableProductos").append(
                "<tr>"+
                    "<th scope='row'>"+ json[i].id_producto +"</th>"+
                    "<td>"+ json[i].producto +"</td>"+
                    "<td>"+
                        "<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_producto+"' onclick='detalleProducto("+json[i].id_producto+",`"+json[i].producto+"`)'>Detalles</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_producto+"' onclick='modificarProducto("+json[i].id_producto+",`"+json[i].producto+"`,"+json[i].precom+","+json[i].preven+",`"+json[i].descripcion+"`)'>Modificar</button>&nbsp;"+
                        "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_producto+"' onclick='eliminarProducto("+json[i].id_producto+",`"+json[i].producto+"`)'>Eliminar</button>"+
                    "</td>"+
                "</tr>"
                );                                        
        }
    })

}

detalleProducto = function(id, producto){
    //console.log(id)    
    vaciarCampos()
    $("#detalleProductoMostrar").append(producto)
    $.ajax({
        method:"POST",
        url:"./accionDetalleProducto.php",
        data:{id:id}
    }).done(function(data){        
        //console.log(data)        
        var json = JSON.parse(data);                            
        //console.log(json);                
        $("#nomProDetalle").append(json[0].producto)
        $("#preCompDetalle").append('$'+json[0].precom)
        $("#preCompMayDetalle").append('$'+json[0].preven)
        $("#descDetallePro").append(json[0].descripcion)
        $("#divImgDetalle").append("<center><img src='../images/"+json[0].imagen+"' width='80%'></center>")
        $("#catDetalleProd").append(json[0].id_categoria[0].categoria);
    }) 
    
}

eliminarProducto = function(id, producto){
    $("#comprobarEliminarProducto").empty();
    $("#comprobarEliminarProducto").append(producto); 
    $("#EliminarProdu").val(id); 
}

eliminarProductoDefinitivo = function(){
    var id_elim = $("#EliminarProdu").val();
    //console.log(id_elim)
    $.ajax({
        method:"POST",
        url:"./accionEliminarProducto.php",
        data:{
            id:id_elim
        }
    }).done(function(data){        
        console.log(data)
        if(data == 0){            
            window.location = "admin.php";
        }else{
            functionError("Ocurrio un error");
        }
    })   
}
    modificarProducto = function(id, producto, precom, preven, descripcion){
        $("#productoModifica").empty();
        $("#productoModifica").append(producto);
        $("#nuevoPrecioCompra").val(precom);
        $("#nuevoPrecioMayoreo").val(preven);
        $("#nuevaDescripcionProducto").val(""+descripcion+"");
        $("#idProductoModificar").val(id);
    }
    
    modificarProductoDefinitiva = function(){    
        var nPrecioCompra = $("#nuevoPrecioCompra").val();
        var nPrecioMayoreo = $("#nuevoPrecioMayoreo").val();
        var nDescripcion = $("#nuevaDescripcionProducto").val();
        if(nPrecioCompra.length == 0 || nPrecioMayoreo.length == 0 || nPrecioCompra.length == 0){
            functionError("No puede haber datos vacíos.");
        }else{
            $.ajax({
                method:"POST",
                url:"./accionModProducto.php",
                data:{
                    precioCompra:nPrecioCompra,
                    precioMayoreo:nPrecioMayoreo,
                    descripcion:nDescripcion,
                    idModificar:$("#idProductoModificar").val()
                }
            }).done(function(data){        
                if(data == 0){                  
                    functionSuccess();              
                    window.location = "admin.php";
                }else if(data == 1){
                    functionError("Ocurrrió un error, intente más tarde")
                }
            }) 
        }
    }


$("#uploadForm").on('submit',(function(e) {
    e.preventDefault();
    //console.log(this.nombreProducto.value)    
    $.ajax({
        url: "upload.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
        {
            //$("#targetLayer").html(data);
            console.log(data[0])            
            if(data[0]==0){
                functionSuccess("Se registro el producto de manera correcta");
                window.location = "admin.php";
            }else{
                functionError("Intente más tarde");
            }
        },
        error: function() 
        {
        } 	        
    });
}));  


functionSuccess = function(success){
    Swal.fire(
        'Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    /*Swal.fire(
        'Cargando...',
        'Espere un momento',
        'info'
    )*/
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando los datos de acceso...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

functionError = function(error){
    Swal.fire(
        'Ocurrio un error',
        error,
        'error'
    )    
}

vaciarCampos = function(){
    $("#detalleProductoMostrar").empty();
    $("#nomProDetalle").empty()
    $("#preCompDetalle").empty()
    $("#preCompMayDetalle").empty()
    $("#descDetallePro").empty()
    $("#divImgDetalle").empty();
}