registrarAlmacen = function(){
    var producto = $("#selectProducto").val();
    var entrada = $("#entrada").val();
    var salida = $("#salida").val();
    var fecha = $("#fecha").val();
    console.log("Llega a la función");
    $.ajax({
        method:"POST",
        url:"./accionRegAlmacen.php",
        data:{
            productoS:producto,
           	entradaS:entrada,                        
            salidaS:salida,
            fechaS:fecha
        }
    }).done(function(data){  
    console.log("Llega a data " + data);    
        if(data == 0){
            functionSucces("Almacén registrado con exito.");
            vaciarCampos();
            window.location = "almacen.php";
        }else if(data == 1){
            functionError("Ocurrrió un error, intente más tarde")
        }else if(data == 2){
            functionError("Ocurrrió un error, intente más tarde")
        }     
    })
}
detalleAlmacen = function(id){   
    vaciarCamposModal();
        $("#detalleAlmacenMostrar").append(id)
        $.ajax({
            method:"POST",
            url:"./accionDetalleAlmacen.php",
            data:{id:id}
        }).done(function(data){      
            //console.log(data)        
            var json = JSON.parse(data);  
            console.log(json)                                   
            $("#productoDetalle").append(json[0].id_producto[0].producto);
            $("#entradaDetalle").append(json[0].entrada);
            $("#salidaDetalle").append(json[0].salida);
            $("#fechaDetalle").append(json[0].fecha);            
        }) 
        
    }
    eliminarAlmacen = function(id){
        $("#comprobarEliminarAlmacen").empty();
        $("#comprobarEliminarAlmacen").append(id); 
        $("#eliminarAlmacen").val(id); 
    }
    eliminarAlmacenDefinitivo = function(){
        var id_eliminar = $("#eliminarAlmacen").val();
        $.ajax({
            method:"POST",
            url:"./accionEliminarAlmacen.php",
            data:{
                id:id_eliminar
            }
        }).done(function(data){        
            console.log(data)
            if(data == 0){
                functionSucces("Almacén eliminado con éxito.");        
                window.location = "almacen.php";
            }else{
                functionError("Ocurrió un error");
            }
        })   
    }
    modificarAlmacen = function(id, entradas, salidas){
        $("#almacenModifica").empty();
        $("#almacenModifica").append(id);
        $("#idAlmacenModificar").val(id); 
        $("#nuevasEntradas").val(entradas);
        $("#nuevasSalidas").val(salidas);
    }
    
    modificarAlmacenDefinitiva = function(){    
        var nEntradas = $("#nuevasEntradas").val();
        var nSalidas = $("#nuevasSalidas").val();
        $.ajax({
            method:"POST",
            url:"./accionModAlmacen.php",
            data:{
                entrada:nEntradas,
                salida:nSalidas,
                idModificar:$("#idAlmacenModificar").val()
            }
        }).done(function(data){        
            //console.log(data)
            if(data == 0){                                
                window.location = "almacen.php";
            }else if(data == 1){
                functionError("Ocurrrió un error, intente más tarde")
            }
        })       
    }
functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
    )    
}

functionSucces = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    $("#selectProducto").val("");
    $("#entrada").val("");
    $("#salida").val("");
    $("#fecha").val("");
}
vaciarCamposModal = function(){
    $("#detalleAlmacenMostrar").empty();
    $("#productoDetalle").empty();
    $("#entradaDetalle").empty();
    $("#salidaDetalle").empty();
    $("#fechaDetalle").empty();
}