detalleImagen = function(id, idProducto){   
    vaciarCampos();
    $("#detalleImagenMostrar").append(id)
    //$("#productoDetalle").append(idProducto)
    $.ajax({
        method:"POST",
        url:"./accionDetalleImagen.php",
        data:{id:id,
            id_producto:idProducto}
    }).done(function(data){        
        //console.log(data)        
        var json = JSON.parse(data);                            
        console.log(json);                
        //console.log(json[0].id_producto[0].producto)        
        $("#productoDetalle").append(json[0].id_producto[0].producto);        
        //$("#statusDetalle").append(json[0].status);
        $("#divImgDetalle").append("<center><img src='../images/"+json[0].imagen+"' width='80%'></center>");
        if(json[0].tipo == 1){
            $("#statusDetalle").append("Habilitado");
        }else{  
            $("#statusDetalle").append("Deshabilitado");
        }
        if(json[0].tipoDetalle == 1){
            $("#tipoDetalle").append("Habilitado");
        }else{
            $("#tipoDetalle").append("Deshabilitado");
        }
    }) 
    
}
$("#registrarImagen").on('submit',(function(e) {
    e.preventDefault();
    //console.log(this.nombreProducto.value)    
    $.ajax({
        url: "accionRegImagen.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
        {
            //$("#targetLayer").html(data);            
            if(data[0]==0){
                functionSuccess("Se registró el producto de manera correcta");
                window.location = "imagen.php";
            }else{
                console.log(data);
                functionError("Intente más tarde");
            }
        },
        error: function() 
        {
        }           
    });
}));
/*
$("#modificarImagen1").on('submit',(function(e) {
    e.preventDefault();   
    $.ajax({
        url: "accionModImagen.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
        {
            console.log(data[0])            
            if(data[0]==0){
                functionSuccess("Se actualizó el producto de manera correcta");
                window.location = "imagen.php";
            }else{
                functionError("Intente más tarde");
            }
        },
        error: function() 
        {
        }           
    });
}));

------A VER SI DESPUÉS LO USAMOS, SI NO ALV----------


*/
modificarImagen = function(id){
    $("#imagenModifica").empty();
    $("#imagenModifica").append(id);
    $("#idImagenModificar").val(id);
    $("#idModifica").val(id);
}
/*
modificarImagenDefinitiva = function(){    
    //var nImagen = $("#nuevaImagen").val();
    var nImagen = document.getElementsByName('nuevaImagen');
    var nProducto = $("#nuevoProducto").val();
    var nTipo = $("#nuevoTipo").val();
    var nStatus = $("#nuevoStatus").val();
    console.log(nImagen);
    console.log(nProducto);
    console.log(nTipo);
    console.log(nStatus);
    $.ajax({
        method:"POST",
        url:"./accionModImagen.php",
        data:{
            imagen:nImagen,
            producto:nProducto,
            tipo:nTipo,
            status:nStatus,
            idModificar:$("#idImagenModificar").val()
        }
    }).done(function(data){        
        console.log(data)
        if(data == 0){
            functionSuccess("Imagen actualizada exitosamente");                              
            window.location = "imagen.php";
        }else if(data == 1){
            functionError("Ocurrrió un error, intente más tarde")
        }
    })       
}*/
eliminarImagen = function(id){
    $("#comprobarEliminarImagen").empty();
    $("#comprobarEliminarImagen").append(id); 
    $("#eliminarImagen").val(id); 
}
eliminarImagenDefinitivo = function(){
    var id_eliminar = $("#eliminarImagen").val();
    $.ajax({
        method:"POST",
        url:"./accionEliminarImagen.php",
        data:{
            id:id_eliminar
        }
    }).done(function(data){        
        console.log(data)
        if(data == 0){       
            functionSuccess("Imagen eliminada correctamente.");     
            window.location = "imagen.php";
        }else{
            functionError("Ocurrió un error");
        }
    })   
}

function seleccionarImagen(){
    var select = $("#selectTipo").val();
    console.log(select);
    if(select == 0){
        document.getElementById("selectImagen").innerHTML = "<input type='file' name='userImage[]' class='form-control' required multiple>";
    }else{
        document.getElementById("selectImagen").innerHTML = "<input type='file' name='userImage' class='form-control' required>";
    }
}
functionSuccess = function(success){
    Swal.fire(
        'Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    /*Swal.fire(
        'Cargando...',
        'Espere un momento',
        'info'
    )*/
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando los datos de acceso...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

functionError = function(error){
    Swal.fire(
        'Ocurrio un error',
        error,
        'error'
    )    
}

vaciarCampos = function(){
    $("#detalleImagenMostrar").empty();
    $("#productoDetalle").empty()
    $("#tipoDetalle").empty()
    $("#statusDetalle").empty()
    $("#divImgDetalle").empty();
}