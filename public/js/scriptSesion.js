
alert = function(){
    Swal.fire(
        'Good job!',
        'You clicked the button!',
        'success'
        )
}

registrarUsuario = function(){
    var correoRegistro = $("#emailRegistrarse").val();
    var contraseniaRegistro = $("#contraseniaReg").val();
    var confirmaContraseniaRegistro = $("#confirmarContrasenia").val();
    
    if(correoRegistro != "" && contraseniaRegistro != "" && confirmaContraseniaRegistro != ""){        
        if(contraseniaRegistro.length < 8 ){
            functionError("Las contraseñas debe tener por lo menos 8 digitos")
        }else{
            if(contraseniaRegistro != confirmaContraseniaRegistro){
                functionError("Las contraseñas no coinciden, reviselas por favor")
            }else{                
                $.ajax({
                    method:"POST",
                    url:"./accionRegUsuario.php",
                    data:{
                        correo:correoRegistro,
                        pass:contraseniaRegistro,                        
                        status:1,
                        nevel:0
                    }
                }).done(function(data){        
                    //console.log(data)
                    if(data == 0){
                        functionSuccess("Usuario registrado con exito");
                        vaciarCampos();
                    }else if(data == 1){
                        functionError("Usuario existente, verifique su correo")
                    }else if(data == 2){
                        functionError("Ocurrrio un error, intente más tarde")
                    }     
                })
            }
        }        
    }else{
        functionError("Revise los campos solicitados");
    }
}


iniciarSesion = function(){
    var correoSesion = $("#emailIniciarSesion").val();
    var contraseniaSesion = $("#contrasenia").val();
    if(correoSesion != "" && contraseniaSesion !=""){
        $.ajax({
            method:"POST",
            url:"./accionIniciarSesion.php",
            data:{
                correo:correoSesion,
                pass:contraseniaSesion
            },beforeSend: function(){
                functionWait();
            }
        }).done(function(data){        
            console.log(data)
            if(data == 0){
                functionError("Usuario inexistente, verifique su correo");
                vaciarCampos();
            }else if(data == 1){
                functionError("Revise su contraseña")
            }else if(data == 20){                
//<<<<<<< HEAD
window.location.href='../index.php'
//=======
                //window.location.href='./index.php'
//>>>>>>> 3f8384255c821f59f889dd4e8501206f06d71031
console.log("user")
}else if(data == 21){                
    window.location.href='admin.php'
    console.log("secre")
}else if(data == 22){                
    window.location.href='admin.php'
    console.log("admin")
}
})
    }else{
        functionError("Revise los campos solicitados");
    }
}

iniciarSesionCarrito = function(){
    var correoSesion = $("#emailIniciarSesion").val();
    var contraseniaSesion = $("#contrasenia").val();
    console.log(correoSesion + " " + contraseniaSesion)
    if(correoSesion != "" && contraseniaSesion !=""){
        $.ajax({
            method:"POST",
            url:"./acciones/accionIniciarSesion.php",
            data:{
                correo:correoSesion,
                pass:contraseniaSesion
            },beforeSend: function(){
                functionWait();
            }
        }).done(function(data){        
            console.log(data)
            if(data == 0){
                functionError("Usuario inexistente, verifique su correo");
                vaciarCampos();
            }else if(data == 1){
                functionError("Revise su contraseña")
            }else if(data == 20){                
                window.location.reload()
                console.log("user")
            }else if(data == 21){                
                //window.location.href='admin.php'
                console.log("secre")
            }else if(data == 22){                
                window.location.href='admin.php'
                console.log("admin")
            }
        })
    }else{
        functionError("Revise los campos solicitados");
    }
}
enviarCorreoContrasenia = function(){
    var correo = $("#correo").val();
    console.log(correo);
    $.ajax({
        method:"POST",
        url:"./accionReestablecerContrasenia.php",
        data:{
            mail:correo
        }
    }).done(function(data){  
        console.log(data);      
        if(data == 0){                  
            functionSuccess("La contraseña se a cambiado de forma exitosa. Favor de abrir su correo electrónico.");              
            window.location = "../index.php";
        }else if(data == 1){
            functionError("Ocurrrió un error, intente más tarde")
        }
    })
}
cambiarPassword = function(correo){
    //var correo = correo;
    //console.log(correo);
    var passwordActual = $("#passwordActual").val();
    var passwordNueva = $("#passwordNueva").val();
    var passwordConfirm = $("#passwordConfirm").val();
    if(passwordActual == "" || passwordNueva == "" || passwordConfirm == ""){
        functionError("Favor de llenar todos los campos.");
    }else{ 
        if(passwordNueva != passwordConfirm){
            functionError("Las contraseñas no coinciden, favor de verificar su información.");
        }else{
            $.ajax({
                method:"POST",
                url:"./accionModContrasenia.php",
                data:{
                    password:passwordActual,
                    passwordN:passwordNueva
                }
            }).done(function(data){  
                console.log(data);      
                if(data == 0){                  
                    functionSuccess("La contraseña se a cambiado de forma exitosa.");              
                    window.location = "../index.php";
                }else if(data == 1){
                    functionError("Ocurrrió un error, intente más tarde")
                }
            })
        }
    }
}

functionError = function(error){
    Swal.fire(
        'Ocurrio un error',
        error,
        'error'
        )    
}

functionSuccess = function(success){
    Swal.fire(
        'Exito!',
        success,
        'success'
        )
}

functionWait = function(){
    /*Swal.fire(
        'Cargando...',
        'Espere un momento',
        'info'
        )*/
        Swal.fire({
            title: '<strong> Espere un momento.</strong>',
            type: 'info',
            html:
            'Est&aacute;mos confirmando los datos de acceso...',
            showCloseButton: false,
            showCancelButton: false,
            showConfirmButton:false
        })
}

vaciarCampos = function(){
    $("#emailRegistrarse").val("");
    $("#contraseniaReg").val("");
    $("#confirmarContrasenia").val("");
}