window.onload = function(){    
    $.ajax({
        method:"POST",
        url:"./accionMostrarCategoria.php",
        data:{}
    }).done(function(data){        
        //console.log(data)        
        var json = JSON.parse(data);                     
        //console.log(json);        
        for(var i=0; i < json.length;i++){            
            $("#bodyCategorias").append(
                "<tr>"+
                    "<th scope='row'>"+ json[i].id_categoria +"</th>"+
                    "<td>"+ json[i].categoria +"</td>"+
                    "<td>"+
                        //"<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_categoria+"' onclick='detalleCategoria("+json[i].id_categoria+")'>Detalles</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_categoria+"' onclick='modificarCategoria("+json[i].id_categoria+",`"+json[i].categoria+"`)'>Modificar</button>&nbsp;"+
                        "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_categoria+"' onclick='eliminarCategoria("+json[i].id_categoria+",`"+json[i].categoria+"`)'>Eliminar</button>"+
                    "</td>"+
                "</tr>"
                );                                   
        }
    })  
}

registrarCategoria = function(){
    var nombreCat = $("#nombreRegCat").val();    
    if(nombreCat!="" && nombreCat.length > 2){
        $.ajax({
            method:"POST",
            url:"./accionRegCategoria.php",
            data:{
                nombre:nombreCat
            }
        }).done(function(data){        
            //console.log(data)
            if(data == 0){                                
                window.location = "categorias.php";
            }else if(data == 1){
                functionError("Categoría existente, verifique el nombre")
            }else if(data == 2){
                functionError("Ocurrrio un error, intente más tarde")
            }     
        })        
    }else{
        functionError("Revise el nombre de la categoría, al menos debe tener 5 caracteres");
    }
}

modificarCategoria = function(id, categoria){
    $("#catModifica").empty();
    $("#catModifica").append(categoria);
    $("#idCatModificar").val(id); 
    $("#nuevoNombreCat").val(categoria); 
}

modificarCategoriaDefinitiva = function(){    
    var nNombre = $("#nuevoNombreCat").val();
    $.ajax({
        method:"POST",
        url:"./accionModCategoria.php",
        data:{
            nombre:nNombre,
            idModificar:$("#idCatModificar").val()
        }
    }).done(function(data){        
        //console.log(data)
        if(data == 0){                                
            window.location = "categorias.php";
        }else if(data == 1){
            functionError("Ocurrrio un error, intente más tarde")
        }
    })       
}

eliminarCategoria = function(id, categoria){   
    //$("#cerrarModalEliminar").empty();
    $("#comprobarEliminar").empty();
    $("#comprobarEliminar").append(categoria); 
    $("#EliminarCate").val(id);      
}

eliminarCategoriaDefinitiva = function(){
    var id_elim = $("#EliminarCate").val();
    $.ajax({
        method:"POST",
        url:"./accionEliminarCategoria.php",
        data:{
            id:id_elim
        }
    }).done(function(data){        
        console.log(data)
        if(data == 0){            
            window.location = "categorias.php";
        }else{
            functionError("Ocurrio un error");
        }
    })   
}


functionError = function(error){
    Swal.fire(
        'Ocurrio un error',
        error,
        'error'
    )    
}

functionSuccess = function(success){
    Swal.fire(
        'Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    /*Swal.fire(
        'Cargando...',
        'Espere un momento',
        'info'
    )*/
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando los datos de acceso...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    $("#nombreRegCat").val("");
}