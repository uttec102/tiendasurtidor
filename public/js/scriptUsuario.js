window.onload = function(){     
    $.ajax({
        method:"POST",
        url:"./accionMostrarUsuarios.php",
        data:{}
    }).done(function(data){               
        var json = JSON.parse(data);                     
        //console.log(json);        
        for(var i=0; i < json.length;i++){  
            //console.log(json[i].status)
            if(json[i].status==1){
                $("#tableUsuarios").append(
                    "<tr>"+
                    "<th scope='row'>"+ json[i].id_usuario +"</th>"+
                    "<td>"+ json[i].correo +"</td>"+"<td>Habilitado</td>"+
                    "<td>"+
                    "<button class='btn btn-danger' data-toggle='modal' data-target='#modalEliminar' value='"+json[i].id_usuario+"' onclick='eliminarUsuario("+json[i].id_usuario+",`"+json[i].correo+"`)'>Baja</button>"+
                    "</td>"+
                    "</tr>"
                    );
            }else{
                $("#tableUsuarios").append(
                    "<tr>"+
                    "<th scope='row'>"+ json[i].id_usuario +"</th>"+
                    "<td>"+ json[i].correo +"</td>"+"<td>Deshabilitado</td>"+
                    "<td>"+
                    "<button class='btn btn-primary' value='"+json[i].id_usuario+"' onclick='altaUsuario("+json[i].id_usuario+",`"+json[i].correo+"`)'>Alta</button>"+
                    "</td>"+
                    "</tr>"
                    );
            }       
        }
    })
}

registrarSecretaria = function(){
    var correoSecretaria = $("#correoSecretaria").val();
    var password = $("#password").val();
    var confirmarPassword = $("#passwordConfirm").val();
    var expresionCorreo = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    var expresionContrasenia = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/;
    if(!expresionCorreo.test(correoSecretaria)){
        functionError("Formato inválido para el correo. Favor de revisar sus datos.");
    }else{
        if (!expresionContrasenia.test(password) || !expresionContrasenia.test(confirmarPassword)){
            functionError("Formato inválido para la contraseña, la contraseña debe tener por lo menos una mayúscula," + 
             "una minúscula, un número, ser mayor a 8 caracteres e inferior a 15 y tener un caracter especial");
        }else{
            if(password != confirmarPassword){
                functionError("Las contraseñas no coinciden. Favor de verificar su información.");
            }else{
                $.ajax({
                    method:"POST",
                    url:"./accionRegSecretaria.php",
                    data:{
                        correo:correoSecretaria,
                        password:password,                        
                        passwordConfirm:confirmarPassword
                    }
                        /*processData: false,
                        contentType: "application/json"*/
                    }).done(function(data){ 
                        console.log(data);     
                        if(data == 0){
                            functionSuccess("Secretaria registrado con exito.");
                            vaciarCampos();
                            window.location = "usuarios.php";
                        }else if(data == 1){
                            functionError("Secretaria ya existente, verifique su información.")
                        }else if(data == 2){
                            functionError("Ocurrrió un error, intente más tarde")
                        }     
                    })
                }
            }
        }
    }


    detalleProveedor = function(id, proveedor){
        vaciarCamposModal();       
        $("#detalleProveedorMostrar").append(proveedor)
        $.ajax({
            method:"POST",
            url:"./accionDetalleProveedor.php",
            data:{id:id}
        }).done(function(data){              
            //console.log(data)
            var json = JSON.parse(data);
            //console.log(json[0])
            $("#proveedorDetalle").append(json[0].proveedor);
            $("#contactoDetalle").append(json[0].contacto);
            $("#telefonoPDetalle").append(json[0].telefono1);
            $("#correoPDetalle").append(json[0].correo1);
            $("#telefonoSDetalle").append(json[0].telefono2);
            $("#correoSDetalle").append(json[0].correo2);

            
            //-------------CHECAR EL ESTADO Y ESE PEDO EN LA MODAL-------------

            $("#estadoDetalle").append(json[0].id_estado[0].estado);
            $("#municipioDetalle").append(json[0].id_municipio[0].municipio);
            $("#coloniaDetalle").append(json[0].id_colonia[0].colonia); 
            $("#codigoPostalDetalle").append(json[0].id_codpos[0].codpos);
            $("#calleDetalle").append(json[0].calle);
            /*$("#nExtDetalle").append(json[0].Noext);
            $("#nIntDetalle").append(json[0].Noint);*/
            //IMPORTANTE $("#divImgDetalle").append("<center><img src='../images/"+json[0].imagen+"' width='80%'></center>");
        }) ;
    }

    eliminarUsuario = function(id, proveedor){
        $("#comprobarEliminarUsuario").empty();
        $("#comprobarEliminarUsuario").append(proveedor); 
        $("#eliminarUsuario").val(id);
        console.log(id);
    }

    eliminarUsuarioDefinitivo = function(){
        var id_eliminar = $("#eliminarUsuario").val();
        console.log(id_eliminar);
        $.ajax({
            method:"POST",
            url:"./accionEliminarUsuario.php",
            data:{
                id:id_eliminar
            }
        }).done(function(data){        
            console.log(data)
            if(data == 0){       
                functionSuccess("Usuario deshabilitado correctamente.");     
                window.location = "usuarios.php";
            }else{
                functionError("Ocurrió un error");
            }
        })   
    }
    altaUsuario = function(id, proveedor){
   // console.log(id + proveedor)
   $.ajax({
    method:"POST",
    url:"./accionAltaUsuario.php",
    data:{
        id:id,
    },
    beforeSend: function(){
        functionWait();
    }
}).done(function(data){               
    console.log(data)
    if(data==0){
        functionSuccess("Actualización exitosa")
        window.location = "usuarios.php";
        vaciarCampos();                
    }else{
        functionError("Intente más tarde")
    }
})
}
modificarProveedor = function(id, proveedor, contacto, telefono1, correo1, telefono2, correo2){
    $("#proveedorModifica").empty();
    $("#proveedorModifica").append(proveedor);
    $("#nuevoContacto").val(""+contacto+"");
    $("#nuevoTelefonoP").val(telefono1);
    $("#nuevoCorreoP").val(""+correo1+"");
    $("#nuevoTelefonoS").val(telefono2);
    $("#nuevoCorreoS").val(""+correo2+"");
    $("#idProveedorModificar").val(id);
}

modificarProveedorDefinitiva = function(){    
    var nContacto = $("#nuevoContacto").val();
    var nTelefonoP = $("#nuevoTelefonoP").val();
    var nCorreoP = $("#nuevoCorreoP").val();
    var nTelefonoS = $("#nuevoTelefonoS").val();
    var nCorreoS = $("#nuevoCorreoS").val();
    if(nContacto.length == 0 || nTelefonoP.length == 0 || nCorreoP.length == 0 || nTelefonoS.length == 0 || nCorreoS.length == 0){
        functionError("No puede haber datos vacíos.");
    }else{
        if (nContacto.length < 10){
            functionError("El nombre del contacto debe ser por lo menos de 10 caracteres.");
        }else{
            if(nTelefonoP.length < 12 || nTelefonoP.length >12){
                functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
            }else{
             if(nTelefonoS.length < 12 || nTelefonoS.length >12){
                functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
            }else{
                $.ajax({
                    method:"POST",
                    url:"./accionModProveedor.php",
                    data:{
                        contacto:nContacto,
                        telefonoP:nTelefonoP,
                        correoP:nCorreoP,
                        telefonoS:nTelefonoS,
                        correoS:nCorreoS,
                        idModificar:$("#idProveedorModificar").val()
                    }
                }).done(function(data){        
                    if(data == 0){                  
                        functionSuccess();              
                        window.location = "proveedor.php";
                    }else if(data == 1){
                        functionError("Ocurrrió un error, intente más tarde")
                    }
                }) 
            }      
        }
    }
}
}
functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
        )    
}

functionSuccess = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
        )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    $("#proveedor").val("");
    $("#contacto").val("");
    $("#telefono1").val("");
    $("#correo1").val("");
    $("#telefono2").val("");
    $("#correo2").val("");
    $("#selectEstado").val("");
    $("#selectMunicipio").val("");
    $("#selectColonia").val("");
    $("#selectCodigoPostal").val("");
    $("#calle").val("");
    $("#nExt").val("");
    $("#nInt").val("");
}
vaciarCamposModal = function(){
    $("#detalleProveedorMostrar").empty();
    $("#proveedorDetalle").empty();
    $("#contactoDetalle").empty();
    $("#telefonoPDetalle").empty();
    $("#correoPDetalle").empty();
    $("#telefonoSDetalle").empty();
    $("#correoSDetalle").empty();
    $("#estadoDetalle").empty();
    $("#municipioDetalle").empty();
    $("#coloniaDetalle").empty();
    $("#codigoPostalDetalle").empty();
    $("#calleDetalle").empty();
    $("#nExtDetalle").empty();
    $("#nIntDetalle").empty();
}

altaProveedor = function(id, proveedor){
    //console.log(id + proveedor)
    $.ajax({
        method:"POST",
        url:"./accionAltaProveedor.php",
        data:{
            id:id,
        },
        beforeSend: function(){
            functionWait();
        }
    }).done(function(data){               
        //console.log(data)
        if(data==0){
            functionSuccess("Actualización exitosa")
            window.location = "proveedor.php";
            vaciarCampos()                
        }else if(data==1){
            functionError("Intente más tarde")
        }
    })
}