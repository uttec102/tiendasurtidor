agregarCarrito = function(id){
    //console.log(id)
    if(id != '' && id != null){
        $.ajax({
            method:"POST",
            url:"./acciones/accionAgregarCarrito.php",
            data:{
                producto:id
            },beforeSend: function(){
                functionWait("Agregando producto");
            }
        }).done(function(data){ 
            //console.log(data)  
            if(data == 0){
                functionError("Producto repetido");                
            }else if(data == 1){
                functionSucces("Producto agregado")
                $("#cart-quantity").empty()
                $("#cart-quantity").append(1);
            }else if(data == 2){                
                functionError("Ocurrio un error al agregar el producto")                
            }
        })
    }else{
        functionError("Ocurrio un error, producto invalido")
    }
}

vaciarCarrito = function(){    
    $.ajax({
        method:"POST",
        url:"./acciones/accionVaciarCarrito.php",
        data:{

        },beforeSend: function(){
            functionWait("Espere un momento");
        }
    }).done(function(data){ 
        console.log(data)  
        if(data == 1){
            window.location.href = "./index.php"
        }
    })
}

eliminarPedido = function(id){
    console.log(id)
    $.ajax({
        method:"POST",
        url:"./acciones/elimnarProductoCarrito.php",
        data:{
            id:id
        },beforeSend: function(){
            functionWait("Espere un momento");
        }
    }).done(function(data){ 
        console.log(data)  
        if(data == 1){
            window.location.href = "./shopping-cart.php"
        }
    })
}

updateCarrito = function(value,id){
    console.log(value + " " + id)
    $.ajax({
        method:"POST",
        url:"./acciones/accionUpdateCarrito.php",
        data:{
            id:id,
            cantidad:value
        },beforeSend: function(){
            functionWait("Espere un momento");
        }
    }).done(function(data){ 
        console.log(data)  
        if(data == 1){            
            //functionSucces();
            window.location.reload();
        }
    })   
}

codigoPostal = function(codigo_postal){
    //console.log(codigo_postal);
    $.ajax({
        method:"GET",
        url:"https://api-codigos-postales.herokuapp.com/v2/codigo_postal/"+codigo_postal,
        data:{            
        },beforeSend: function(){
            //functionWait("Espere un momento");
        }
    }).done(function(data){ 
        //console.log(data)          
        $("#estadoEntrega").val(data.estado)
        $("#MunicipioEntrega").val(data.municipio)
        data.colonias.forEach(c=>{
            //console.log(c)
            $("#ColoniasEntrega").append("<option value='"+c+"'>"+c+"</option>");
        })                
    }) 
}

realizarVenta = function(){
    var contacto = $("#contactoEntrega").val()    
    var telefono = $("#telefonoEntrega").val()    
    var correo = $("#correoContacto").val()    
    var cp = $("#codigoPostal").val()    
    var estado = $("#estadoEntrega").val()    
    var municipio = $("#MunicipioEntrega").val()    
    var colonia = $("#ColoniasEntrega").val()
    var calle = $("#calleEntrega").val()
    var int = $("#interiorEntrega").val()
    var ext = $("#exteriorEntrega").val()
    var expresionRegular = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    
    if(contacto == "" || telefono == "" || correo == "" || cp == "" || calle == ""){
        functionError("Favor de completar todos los datos obligatorios.");
    }else{
        if (contacto.length < 10){
            functionError("El nombre del contacto debe ser por lo menos de 10 caracteres.");
        }else{
            if(telefono.length != 10){
                functionError("El teléfono no debe ser superior o inferior a 12 digitos.");
            }else{
                if(!expresionRegular.test(correo)){
                    functionError("Formato inválido para el correo. Favor de revisar sus datos.");
                }else{
                    if(cp.length != 5){
                        functionError("El código postal solo consta de 5 digitos.");
                    }else{
                        //AQUÍ TU CÓDIGO DE $.ajax
                        $.ajax({
                            method:"POST",
                            url:"./acciones/accionAgregarCompra.php",
                            data:{
                                contacto:contacto,
                                telefono:telefono,
                                correo:correo,
                                cp:cp,
                                estado:estado,
                                municipio:municipio,
                                colonia:colonia,
                                calle:calle,
                                int:int,
                                ext,ext            
                            },beforeSend: function(){
                                functionWait("Espere un momento");
                            }
                        }).done(function(data){ 
                            //console.log(data)  
                            var json = JSON.parse(data);                     
                            //console.log(json);    
                            
                            //PDF

                            var doc = new jsPDF()
                            var y = 90
                            //var img = new Image()
                            doc.setFont("Arial");
                            doc.setFontSize(15);
                            //img.src = './public/imagenes/logo-footer.png'
                            //pdf.addImage(img, 'png', 10, 78, 12, 15)
                            doc.text('Gracias por realizar una compra', 105, 10, 'center')
                            doc.text('DDTech', 105, 20, 'center')
                            doc.text('ID Compra: ' + json[0].id_venta, 170, 20, 'right')

                            doc.text('Contacto: ', 10, 35, 'left')
                            doc.text(contacto, 10, 40, 'left')

                            doc.text('Teléfono: ', 60, 35, 'left')
                            doc.text(telefono, 60, 40, 'left')

                            doc.text('Correo: ', 115, 35, 'left')
                            doc.text(correo, 115, 40, 'left')

                            doc.text('Dirección de entrega: ', 10, 55, 'left')
                            doc.text("Estado de " + estado + " municipio de" + municipio + " en la colonia de " + colonia, 10, 60, 'left')
                            doc.text("Calle " + calle + " con el No. Int: " + int + " y No. Ext: " + ext, 10, 65, 'left')

                            doc.text('Monto a pagar: $'+json[0].totalCompra, 10, 75, 'left')
                            
                            doc.text('Productos comprados: '+json[1].length, 10, 85, 'left')

                            var data = new Array
                            var row = new Array;
                            var columns = ["Producto", "Cantidad", "Precio", "Imagen"];
                            json[1].forEach(element => {
                                //console.log(element.id_producto)
                                row = [element.id_producto.producto,element.cantidad,element.id_producto.precom]
                                data.push(row)
                            });

                            //console.log(data)

                            /*var columns = ["Id", "Nombre", "Email", "Pais"];
                            var data = [
                                [1, "Hola", "hola@gmail.com", "Mexico"],
                                [2, "Hello", "hello@gmail.com", "Estados Unidos"],
                                [3, "Otro", "otro@gmail.com", "Otro"] 
                            ];*/

                            doc.autoTable(columns,data,
                                { margin:{ top: 90 }}
                                );
                            //doc.addImage(img, 'PNG', 105, 20, 20, 10, "MEDIUM")
                            doc.save('comprobante-ddtech.pdf')
                            
                            //FIN PDF
                            
                            window.location.href = "./acciones/verCompra.php"
                        })
                    }
                }
            }
        }   
    }
    //console.log(contacto + telefono +" " + correo + cp + " " +estado + municipio + colonia + calle + " " +int + " " +ext)
}

functionError = function(error){
    Swal.fire(
        'Ocurrio un error',
        error,
        'error'
        ) 
}

functionWait = function(message){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:message,
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

functionSucces = function(success){
    Swal.fire(
        'Exito!',
        success,
        'success'
        )
}