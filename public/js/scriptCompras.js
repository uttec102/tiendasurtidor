window.onload = function(){
    $.ajax({
        method:"POST",
        url:"./accionMostrardatosCompra.php",
        data:{}
    }).done(function(data){               
        console.log(data)
        var json = JSON.parse(data);    
        //console.log(json)                      
        //console.log(json[0][0].producto)
        for(var i=0; i < json[0].length;i++){            
            //console.log(json[0][i].producto)
            //console.log(json[0][i].id_producto)
            $("#productosExis").append("<option value='"+json[0][i].id_producto+"'>"+json[0][i].producto+"</option>");
        }

        for(var i=0; i < json[1].length;i++){                        
            $("#proveedorExis").append("<option value='"+json[1][i].id_proveedor+"'>"+json[1][i].proveedor+"</option>");
        }

        for(var i=0; i < json[2].length;i++){
            //console.log(json[2][i].status);
            if(json[2][i].status==1){
                $("#tableCompras").append(
                    "<tr>"+
                        "<th scope='row'>"+ json[2][i].id_compra +"</th>"+
                        "<td>"+ json[2][i].id_producto[0].producto +"</td>"+
                        "<td>"+ json[2][i].id_proveedor[0].proveedor +"</td>"+
                        "<td>"+ json[2][i].cantidad +"</td>"+
                        "<td>"+ json[2][i].fecha+"</td>"+
                        "<td>Alta</td>"+
                        "<td>"+
                            /*"<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_producto+"' onclick='detalleProducto("+json[i].id_producto+")'>Detalles</button>&nbsp;"+
                            "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_producto+"' onclick='modificarCategoria("+json[i].id_producto+")'>Modificar</button>&nbsp;"+*/
                            "<button class='btn btn-warning' data-toggle='modal' data-target='#modalEliminar' value='"+json[2][i].id_compra+"' onclick='eliminarCompra("+json[2][i].id_compra+")'>Baja</button>"+
                        "</td>"+
                    "</tr>"
                );
            }else{
                $("#tableCompras").append(
                    "<tr>"+
                        "<th scope='row'>"+ json[2][i].id_compra +"</th>"+
                        "<td>"+ json[2][i].id_producto[0].producto +"</td>"+
                        "<td>"+ json[2][i].id_proveedor[0].proveedor +"</td>"+
                        "<td>"+ json[2][i].cantidad +"</td>"+
                        "<td>"+ json[2][i].fecha+"</td>"+
                        "<td>Baja</td>"+
                        "<td>"+
                            "<button class='btn btn-success' value='"+json[2][i].id_compra+"' onclick='detalleProducto("+json[2][i].id_compra+")'>Alta</button>&nbsp;"+
                            /*"<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_producto+"' onclick='modificarCategoria("+json[i].id_producto+")'>Modificar</button>&nbsp;"+
                            "<button class='btn btn-warning' data-toggle='modal' data-target='#modalEliminar' value='"+json[2][i].id_compra+"' onclick='eliminarCompra("+json[2][i].id_compra+")'>Baja</button>"+*/
                        "</td>"+
                    "</tr>"
                );
            }
            /*console.log(json[2][i].id_proveedor[0].proveedor);*/
           /* $("#tableCompras").append(
                "<tr>"+
                    "<th scope='row'>"+ json[2][i].id_compra +"</th>"+
                    "<td>"+ json[2][i].id_producto[0].producto +"</td>"+
                    "<td>"+ json[2][i].id_proveedor[0].proveedor +"</td>"+
                    "<td>"+ json[2][i].cantidad +"</td>"+
                    "<td>"+ json[2][i].fecha+"</td>"+
                    "<td>"+ json[2][i].status+"</td>"+
                    "<td>"+
                        "<button class='btn btn-success' data-toggle='modal' data-target='#myModal' value='"+json[i].id_producto+"' onclick='detalleProducto("+json[i].id_producto+")'>Detalles</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalModificar' value='"+json[i].id_producto+"' onclick='modificarCategoria("+json[i].id_producto+")'>Modificar</button>&nbsp;"+
                        "<button class='btn btn-warning' data-toggle='modal' data-target='#modalEliminar' value='"+json[2][i].id_compra+"' onclick='eliminarCompra("+json[2][i].id_compra+")'>Baja</button>"+
                    "</td>"+
                "</tr>"
            );*/
        }
        //tableCompras
        /**/ 

    })
}

detalleProducto = function(id){
    //console.log(id)
    $.ajax({
        method:"POST",
        url:"./accionAltaCompra.php",
        data:{
            id:id,
        },
        beforeSend: function(){
            functionWait();
        }
    }).done(function(data){               
        //console.log(data)
        if(data==0){
            functionSuccess("Actualización exitosa")
            window.location = "compras.php";
            vaciarCampos()                
        }else if(data==1){
            functionError("Intente más tarde")
        }
    })
}


registrarCompra = function(){
    var idProducto = $("#productosExis").val();
    var idProveedor = $("#proveedorExis").val();
    var cantidad = $("#cantidadCompra").val();
    //console.log(idProducto + " " + " " + idProveedor + " " + +cantidad)
    if(idProducto != "" && idProveedor != "" && cantidad > 0){
        $.ajax({
            method:"POST",
            url:"./accionRegCompras.php",
            data:{
                idProducto:idProducto,
                idProveedor:idProveedor,
                cantidad:cantidad
            },
            beforeSend: function(){
                functionWait();
            }
        }).done(function(data){               
            //console.log(data)
            if(data==1){
                functionSuccess("Registro exitoso")
                window.location = "compras.php";
                vaciarCampos()                
            }else if(data==0){
                functionError("Intente más tarde")
            }
        })
    }else {
        functionError("Revise los datos del formulario");
    }
}

functionError = function(error){
    Swal.fire(
        'Ocurrió un error',
        error,
        'error'
    )    
}

functionSuccess = function(success){
    Swal.fire(
        '¡Exito!',
        success,
        'success'
    )
}

functionWait = function(){
    Swal.fire({
        title: '<strong> Espere un momento.</strong>',
        type: 'info',
        html:
        'Est&aacute;mos confirmando sus datos...',
        showCloseButton: false,
        showCancelButton: false,
        showConfirmButton:false
    })
}

vaciarCampos = function(){
    $("#productosExis").val('');
    $("#proveedorExis").val('');
    $("#cantidadCompra").val('');
}

eliminarCompra = function(id){
    $("#comprobarEliminarProducto").empty();
    $("#comprobarEliminarProducto").append(id); 
    $("#EliminarProdu").val(id); 
}

eliminarProductoDefinitivo = function(){
    var id_elim = $("#EliminarProdu").val();
    //console.log(id_elim)
    $.ajax({
        method:"POST",
        url:"./accionEliminarCompra.php",
        data:{
            id:id_elim
        },
        beforeSend:function(){
            functionWait()
        }
    }).done(function(data){        
        //console.log(data)
        if(data == 0){            
            window.location = "compras.php";
        }else{
            functionError("Ocurrio un error");
        }
    })   
}
