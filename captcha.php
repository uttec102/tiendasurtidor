<?php 
session_start();
//crear una imagen
$imagen = imagecreate(100,35);

//Colores
$fondo = imagecolorallocate($imagen, 0, 123, 255);
$texto = imagecolorallocate($imagen, 255, 255, 255);

//Valor aleatorio
$aleatorio = rand(100000,999999);

//Rellenar la imagen
imagefill($imagen, 50, 0, $fondo);

//Imprimir random
imagestring($imagen, 80, 0, 0, $aleatorio, $texto);

//Sesion captcha
$_SESSION['captcha'] = $aleatorio;

//Imprimir la imagen
header('Content-type: image/png'.urlencode($texto));
imagepng($imagen);
?>