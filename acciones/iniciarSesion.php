<!DOCTYPE html>
<html>
<head>
	<title>Inicio sesión</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="public/bootstrap.css">-->
	<script src="../public/jquery.min.js"></script>
	<script src="../public/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../public/style.css">
	<!--<script type="text/javascript" src="public/bootstrap.js"></script>-->
	<link rel="stylesheet" href=".././public/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--<link rel="stylesheet" href="./public/ddtech.min.css">-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../public/css/bootstrap.css">
</head>
<body>
	<div class="bg-dark barra-informativa">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-4 text-center">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">	
							<li>
								<a title="Nosotros" href="../nosotros.php">Nosotros</i></a>
							</li>
							<li class="menu-sep hidden-xs">
								|
							</li>

							<li class="nav-item">
								<a title="Blog" href="#">Blog</i></a>
							</li>
							<li class="menu-sep hidden-xs">
								|
							</li>

							<li class="nav-item">
								<a title="Contacto" href="../contacto.php">Contacto</a>
							</li>
						</div>
					</ul>
				</div>
				<div class="col-sm-6 col-md-5 col-lg-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-phone"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-sm-6 col-md-3 col-lg-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li>
								<a href="acciones/iniciarSesion.php">Iniciar sesión</a>
							</li>
							<li>
								|
							</li>
							<li>
								<a href="acciones/iniciarSesion.php">Registrarme</a>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="../public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="../index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="../detalleProducto.php?imagen=<?php echo rand(1, 20);?>">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Tiendas" href="#">TIENDAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Pride" href="#">PRIDE</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Configurar PC" href="#">CONFIGURAR PC</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Powered by Asus" href="#">POWERED BY ASUS</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<li>
						<a title="Carrito" href="#">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>
				</ul>
			</div>
		</div>
	</nav><br>
	<div class="container">
		<div class="form-control">			
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<h3 class="title">Iniciar sesión</h3>
					<hr>
					<p>Bienvenido, entra a tu cuenta</p>
					<label for="correo">Correo electrónico</label>
					<input type="email" class="form-control" id="emailIniciarSesion" name="emailIniciarSesion" placeholder="nombre@dominio.com" required>
					<label for="correo">Contraseña</label>
					<input type="password" class="form-control" id="contrasenia" name="contrasenia" required><br>
					<button type="submit" class="btn btn-primary" onclick="iniciarSesion()" name="iniciarSesion">Iniciar sesión</button>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<h3 class="title">Crear una nueva cuenta</h3>
					<hr>
					<p>Crea tu cuenta de usuario, solo basta tu correo y contraseña</p>
					<label for="correo">Correo electrónico</label>
					<input type="email" class="form-control" id="emailRegistrarse" name="emailRegistrarse" placeholder="nombre@dominio.com">
					<label for="correo">Contraseña</label>
					<input type="password" class="form-control" id="contraseniaReg" name="contraseniaReg">
					<label for="correo">Confirmar contraseña</label>
					<input type="password" class="form-control" id="confirmarContrasenia" name="confirmarContrasenia"><br>
					<button class="btn btn-primary" onclick="registrarUsuario()" name="registrarse">Registrarme</button>
				</div>
			</div>
			<a href=""></a>	
			<button class='btn btn-success' data-toggle='modal' data-target='#modalContrasenia'>¿Olvidaste tu contraseña?</button>		
		</div>
	</div>
	<div class="footer">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="row sinpadding">
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<img src="../public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<img src="../public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<img src="../public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>	
		</div>
		<div class="informacion">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-2 col-lg-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-sm-12 col-md-2 col-lg-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class=" col-sm-12 col-md-4 col-lg-2">
								<center><img src="../public/imagenes/logoFooter.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<img src="../public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>	
	<script type="text/javascript" src="../public/js/jquery-3.0.0.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="../public/js/bootstrap.js"></script>
	<script src="../public/sweetalert.js"></script>
	<script src="../public/js/scriptSesion.js"></script>
	<div id="modalContrasenia" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reestablecer contraseña</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        Favor de introducir su correo electrónico para poder contactarnos con usted. <b id="proveedorModifica"></b>
                    </div><br>
                    <div class="col-sm-12 col-md-12 col-lg-12">
						<label for="correo">Correo electrónico:</label>
    					<input type="mail" class="form-control" id="correo" name="correo" placeholder="Correo electrónico con el que se registró." required>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" value="" onclick="enviarCorreoContrasenia()" id="idUsuarioPassword">Reestablecer</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>