<!DOCTYPE html>
<html>
<head>
	<title>Proveedor</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="public/bootstrap.css">-->
	<script src="../public/jquery.min.js"></script>
	<script src="../public/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../public/style.css">
	<!--<script type="text/javascript" src="public/bootstrap.js"></script>-->
	<link rel="stylesheet" href=".././public/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--<link rel="stylesheet" href="./public/ddtech.min.css">-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../public/css/bootstrap.css">
</head>
<body>
	<?php 
        session_start();
        /*echo $_SESSION['usuario'];
        echo $_SESSION['level'];*/
        if($_SESSION['level'] != 2 && $_SESSION['level'] != 1){        
            header('Location: ./../index.php');
        }
    ?>
	<div class="bg-dark barra-informativa">
		<div class="container">
			<div class="row">
			<div class="col-md-4 text-center">
				<ul class="nav navbar-nav navbar-left text-center menuSuperior">
					<div class="row">	
						<li>
							<a title="Nosotros" href="#">Nosotros</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Blog" href="#">Blog</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Contacto" href="#">Contacto</a>
						</li>
					</div>
				</ul>
			</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-phone"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li>
								<a href="cerrarSesion.php"><i class="fa fa-sign-out"></i>Cerrar sesión</a>
							</li>
							<li>
								|
							</li>
							<li>
								<a href="#"></a>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="../public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="admin.php">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="categorías" href="categorias.php">CATEGORÍAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Compras" href="compras.php">COMPRAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Provedores" href="proveedor.php">PROVEEDORES</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Almacén" href="almacen.php">ALMACÉN</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Imágen" href="imagen.php">IMÁGEN</a>
					</li>
					<?php
					if($_SESSION['level'] == 2){
						?>
						<li class="menu-sep hidden-xs">
							|
						</li>
						<li class="nav-item">
							<a title="Imágen" href="usuarios.php">USUARIOS</a>
						</li>
					<?php
					}
					?>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<!--<li>
						<a title="Carrito" href="#">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>-->
				</ul>
			</div>
		</div>
	</nav><br>
	<div class="container">
		<div class="form-control">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<nav>
	                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
	                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Proveedores</a>
	                        <?php
							if($_SESSION['level'] == 2){
								?>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Agregar proveedor</a>
							<?php
							}
							?>                       
	                    </div>
	                </nav>
	                <div class="tab-content" id="nav-tabContent">
	                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	                        <table class="table table-striped" id="tableProveedores">
	                            <thead>
	                                <tr>
	                                    <th scope="col">ID</th>
	                                    <th scope="col">Proveedor</th>
										<th scope="col">Contacto</th>                                                              
										<th scope="col">Estatus</th>                                                              
										<th scope="col">Acciones</th>                                                              
	                                </tr>
	                            </thead>
	                            <tbody>
	                            </tbody>
	                        </table>                        
	                    </div>
	                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
	                        <div class="row">
	                        	<div class="col-sm-12 col-md-12 col-lg-12">
		                            <h3 class="title">Registrar nuevo proveedor</h3>
									<hr>
									<label for="correo">Proveedor *</label>
				    				<input type="text" class="form-control" id="proveedor" name="proveedor" placeholder="nombre del proveedor (empresa)" required>
				    				<label for="correo">Nombre completo del contacto *</label>
				    				<input type="text" class="form-control" id="contacto" name="contacto" placeholder="nombre de la persona encargada" required>
				    				<div class="row">
					    				<div class="col-sm-9 col-md-6 col-lg-6">	
						    				<label for="correo">Teléfono principal *</label>
						    				<input type="number" class="form-control" id="telefono1" name="telefono1" placeholder="deben ser 12 digitos" required>
					    				</div>
										<div class="col-sm-9 col-md-6 col-lg-6">	
											<label for="correo">Correo principal *</label>
						    				<input type="email" class="form-control" id="correo1" name="correo1" placeholder="nombre@dominio.com" required>
					    				</div>
				    				</div>
				    				<div class="row">
					    				<div class="col-sm-9 col-md-6 col-lg-6">	
						    				<label for="correo">Teléfono secundario *</label>
						    				<input type="number" class="form-control" id="telefono2" name="telefono2" placeholder="deben ser 12 digitos" required>
					    				</div>
										<div class="col-sm-9 col-md-6 col-lg-6">	
											<label for="correo">Correo secundario *</label>
						    				<input type="email" class="form-control" id="correo2" name="correo2" placeholder="nombre@dominio.com" required>
					    				</div>
				    				</div>
				    				<div class="row">
				    					<div class="col-sm-9 col-md-3 col-lg-3">
				    						<label for="selectCiudad">Estado:</label>
											<select name="estado" id="selectEstado" class="form-control">
												<option value="default" selected>Elige un estado</option>
											</select>
				    					</div>
				    					<div class="col-sm-9 col-md-3 col-lg-3">
				    						<label for="selectMunicipio">Municipio:</label>
											<select name="municipio" id="selectMunicipio" class="form-control">
												<option value="default" selected>Elige un municipio</option>
											</select>
				    					</div>
				    					<div class="col-sm-9 col-md-3 col-lg-3">
				    						<label for="selectColonia">Colonia:</label>
											<select name="colonia" id="selectColonia" class="form-control">
												<option value="default" selected>Elige una colonia</option>
											</select>
				    					</div>
				    					<div class="col-sm-9 col-md-3 col-lg-3">
				    						<label for="selectCódigoPostal">Código postal:</label>
											<select name="codigo" id="selectCodigoPostal" class="form-control">
												<option value="default" selected>Elige un código postal</option>
											</select>
				    					</div>
				    				</div>
				    				<div class="row">
				    					<div class="col-sm-12 col-md-6 col-lg-6">
				    						<label for="correo">Calle *</label>
				    						<input type="text" class="form-control" id="calle" name="calle" placeholder="Calle" required>
				    					</div>
				    					<div class="col-sm-6 col-md-3 col-lg-3">
				    						<label for="correo">No. Exterior</label>
				    						<input type="text" class="form-control" id="nExt" name="nExt" placeholder="Número exterior" required>
				    					</div>
				    					<div class="col-sm-6 col-md-3 col-lg-3">
				    						<label for="correo">No. Interior</label>
				    						<input type="text" class="form-control" id="nInt" name="nInt" placeholder="Número interior" required>
				    					</div>
				    				</div>
				    				<button type="submit" class="btn btn-primary" name="registrarProveedor" id="registrarProveedor" onclick="registrarProveedor()">Registrar proveedor</button>
	                        	</div>
	                        </div>
	                    </div>	                    
	                </div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg1.png" width="100%">
							<div class="centered">
						<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>	
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="./../public/imagenes/logoFooter.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="../public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../public/js/jquery-3.0.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	<script src="../public/js/bootstrap.js"></script>
	<script src="../public/sweetalert.js"></script>
	<script src="../public/js/scriptProveedor.js"></script>
	<script src="../public/js/administrarProveedor.js"></script>
	<!--Modales-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detalles del proveedor: <b id="detalleProveedorMostrar"></b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Proveedor: <b id="proveedorDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Contacto : <b id="contactoDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Teléfono principal: <b id="telefonoPDetalle"></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Correo principal: <b id="correoPDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Teléfono secundario : <b id="telefonoSDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Correo secundario: <b id="correoSDetalle"></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-md-3 col-lg-3">
								<p>Estado: <b id="estadoDetalle"></b></p>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<p>Municipio : <b id="municipioDetalle"></b></p>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<p>Colonia: <b id="coloniaDetalle"></b></p>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<p>Código postal: <b id="codigoPostalDetalle"></b></p>
							</div>
						<!--<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12" id="divImgDetalle">
							
							</div>
						</div>-->
	                	</div>
					</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>

    <!--Modal de eliminar-->
    <div id="modalEliminar" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
					<h4 class="modal-title">Confirmacion para baja</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>                    
                </div>
                <div class="modal-body">
					<p>¿Está seguro de dar de baja al proveedor <b id="comprobarEliminarProveedor"></b>?</p>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" value="" onclick="eliminarProveedorDefinitivo()" id="eliminarProveedor">Baja</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>

        </div>
    </div>

    <!--Modal de Modificar-->
    <div id="modalModificar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ingrese los nuevos datos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        Proveedor a modificar: <b id="proveedorModifica"></b>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
						<label for="correo">Nombre completo del contacto nuevo</label>
    					<input type="text" class="form-control" id="nuevoContacto" name="contacto" placeholder="nombre de la persona encargada" required>
					</div>
					<div class="row">
	    				<div class="col-sm-12 col-md-6 col-lg-6">	
		    				<label for="correo">Teléfono principal *</label>
		    				<input type="number" class="form-control" id="nuevoTelefonoP" name="telefono1" placeholder="deben ser 12 digitos" required>
	    				</div>
						<div class="col-sm-12 col-md-6 col-lg-6">	
							<label for="correo">Correo principal *</label>
		    				<input type="email" class="form-control" id="nuevoCorreoP" name="correo1" placeholder="nombre@dominio.com" required>
	    				</div>
					</div>
					<div class="row">
	    				<div class="col-sm-12 col-md-6 col-lg-6">	
		    				<label for="correo">Teléfono secundario *</label>
		    				<input type="number" class="form-control" id="nuevoTelefonoS" name="telefono2" placeholder="deben ser 12 digitos" required>
	    				</div>
						<div class="col-sm-12 col-md-6 col-lg-6">	
							<label for="correo">Correo secundario *</label>
		    				<input type="email" class="form-control" id="nuevoCorreoS" name="correo2" placeholder="nombre@dominio.com" required>
	    				</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" value="" onclick="modificarProveedorDefinitiva()" id="idProveedorModificar">Modificar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>