<!--<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>-->
    <?php 
        session_start();
        //echo $_SESSION['usuario'];
        //echo $_SESSION['level'];
        if($_SESSION['level'] != 2 && $_SESSION['level'] != 1){        
            header('Location: ./../index.php');
        }
    ?>
    <!--admin
</body>
</html>-->

<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="public/bootstrap.css">-->
	<script src="../public/jquery.min.js"></script>
	<script src="../public/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../public/style.css">
	<!--<script type="text/javascript" src="public/bootstrap.js"></script>-->
	<link rel="stylesheet" href=".././public/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--<link rel="stylesheet" href="./public/ddtech.min.css">-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../public/css/bootstrap.css">
</head>
<body>
	<div class="bg-dark barra-informativa">
		<div class="container">
			<div class="row">
				<div class="col-md-4 text-center">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">	
							<li>
								<a title="Nosotros" href="#">Nosotros</i></a>
							</li>
							<li class="menu-sep hidden-xs">
								|
							</li>

							<li class="nav-item">
								<a title="Blog" href="#">Blog</i></a>
							</li>
							<li class="menu-sep hidden-xs">
								|
							</li>

							<li class="nav-item">
								<a title="Contacto" href="#">Contacto</a>
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-phone"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li>
								<a href="cerrarSesion.php"><i class="fa fa-sign-out"></i>Cerrar sesión</a>
							</li>
							<li>
								|
							</li>
							<li>
								<a href="#"></a>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="../public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="admin.php">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="categorías" href="categorias.php">CATEGORÍAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Compras" href="compras.php">COMPRAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Provedores" href="proveedor.php">PROVEEDORES</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Almacén" href="almacen.php">ALMACÉN</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Imágen" href="imagen.php">IMÁGEN</a>
					</li>
					<?php
					if($_SESSION['level'] == 2){
						?>
						<li class="menu-sep hidden-xs">
							|
						</li>
						<li class="nav-item">
							<a title="Imágen" href="usuarios.php">USUARIOS</a>
						</li>
					<?php
					}
					?>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<!--<li>
						<a title="Carrito" href="#">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>-->
				</ul>
			</div>
		</div>
	</nav><br>
	<div class="container">					
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<nav>
					<div class="nav nav-tabs" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Productos</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Agregar un producto</a>                        
					</div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<table class="table table-striped">
							<thead>
								<tr>
									<th scope="col">ID</th>
									<th scope="col">Nombre</th>                                    
									<th scope="col">Acciones</th>                                                                    
								</tr>
							</thead>
							<tbody id="tableProductos">                                
							</tbody>
						</table>                        
					</div>
					<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
						<form id="uploadForm" action="upload.php" method="post">
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Nombre del producto</label>
									<input type="text" class="form-control" name="nombreProducto" placeholder="Nombre del producto" required>                                
								</div>                            
							</div>
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Precio de compra</label>
									<input type="number" class="form-control" name="precioCompra" placeholder="$1000.00" required >                                
								</div>
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Precio de mayoreo</label>
									<input type="number" class="form-control" name="precioMayoreo" placeholder="$1000.00" required>                                
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Descripción</label>                                                                
									<textarea id="" cols="30" rows="10" name="descripcionProducto" class="form-control" required placeholder="Ingresa una descripción"></textarea>
								</div>
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Categoría</label>
									<select name="selectCategoria" class="form-control" id="categoriaProducto" required>
										<option value="" selected>Selecciona una categoría</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<label for="">Imagen</label>                                                                
									<input type="file" name="userImage" class="form-control" required>
								</div>
								<div class="col-sm-6 col-md-6 col-lg-6">                            
									<button class="btn btn-block btn-success">Registrar</button>
								</div>
							</div>
						</form>
					</div>                    
				</div>					
			</div>
		</div>					
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="../public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>	
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="./../public/imagenes/logoFooter.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="../public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../public/js/jquery-3.0.0.js"></script>	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="../public/js/bootstrap.js"></script>	
	<script src="../public/js/scriptAdmin.js"></script>
	<script src="../public/sweetalert.js"></script>

	<!--Modales-->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Detalles del producto: <b id="detalleProductoMostrar"></b></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>                    
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Nombre producto: <b id="nomProDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Precio de compra : <b id="preCompDetalle"></b></p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4">
							<p>Precio de mayoreo: <b id="preCompMayDetalle"></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-7 col-md-7 col-lg-7">
							<p>Descripción: <b id="descDetallePro"></b></p>
						</div>
						<div class="col-sm-5 col-md-5 col-lg-5">
							<p>Categoría: <b id="catDetalleProd"></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12" id="divImgDetalle">

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>

		</div>
	</div>

	<!--Modal de eliminar-->
	<div id="modalEliminar" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Confirmacion para eliminar</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>                    
				</div>
				<div class="modal-body">
					<p>¿Está seguro de eliminar la categoría <b id="comprobarEliminarProducto"></b>?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" value="" onclick="eliminarProductoDefinitivo()" id="EliminarProdu">Eliminar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>

		</div>
	</div>

	<!--Modal de Modificar-->
	<div id="modalModificar" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Ingrese los nuevos datos</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>                    
				</div>
				<div class="modal-body">
					<div class="col-sm-12 col-md-12 col-lg-12">
						Producto a modificar: <b id="productoModifica"></b>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6">                            
							<label for="">Precio de compra</label>
							<input type="number" class="form-control" name="nuevoPrecioCompra" id="nuevoPrecioCompra" placeholder="$1000.00" required >                                
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">                            
							<label for="">Precio de mayoreo</label>
							<input type="number" class="form-control" name="nuevoPrecioMayoreo" id="nuevoPrecioMayoreo" placeholder="$1000.00" required>                                
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12">                            
							<label for="">Descripción</label>                                                                
							<textarea id="nuevaDescripcionProducto" cols="30" rows="10" name="nuevaDescripcionProducto" class="form-control" required placeholder="Ingresa una descripción"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal" value="" onclick="modificarProductoDefinitiva()" id="idProductoModificar">Modificar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

