<script type="text/javascript">
    function mensaje(){
        alert("Gracias por su preferencia, esperemos que haya quedado satisfecho.");
        location.href="index.php";
    }
</script>
<!DOCTYPE html>
 <html>
 <head>
 	<title>Realizar compra</title>
 	<meta charset="utf-8">
 	<link rel="stylesheet" type="text/css" href="public/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="public/style.css">
	<link rel="stylesheet" href="./public/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/css/bootstrap.css">
 </head>
 <body>
 	<?php
    if(isset($_REQUEST["nombre"])){
        $destino = "tiendasurtidor@outlook.com";//su correo
        $asunto = "Compra realizada";
        $mensaje = "La información es: " . PHP_EOL .
        "Nombre: " . $_REQUEST["nombre"] . PHP_EOL .
        "Correo: " . $_REQUEST["correo"] . PHP_EOL .
        utf8_decode("Teléfono: ") . $_REQUEST["telefono"] . PHP_EOL .
        "Comentarios: " . utf8_decode($_REQUEST["comentarios"]);
        $remitente = $_REQUEST["correo"];
        mail($destino, $asunto, $mensaje, $remitente);
        echo "<script>";
        echo "mensaje();";
        echo "</script>";

        $destinoRemitente = $remitente;
        $asuntoRemitente = "Compra realizada en Tienda Surtidor";
        $mensajeRemitente = utf8_decode("Información de compra: ") . PHP_EOL .
        " Mensaje: " . utf8_decode($_REQUEST["comentarios"]) . PHP_EOL .
        " Producto comprado: " . $producto . PHP_EOL .
        utf8_decode(" Gracias por su preferencia, cualquier duda o aclaración favor de enviar sus comentarios en el apartado de contacto.");
        $remitenteNuevo = "tiendasurtidor@outlook.com";
        mail($destinoRemitente, $asuntoRemitente, $mensajeRemitente, $remitenteNuevo);

    }else{
        $imagen = $_GET["confirmarCompra"];
    ?>
 	<div class="bg-dark">
		<div class="container">
			<div class="row">
			<div class="col-md-4 text-center">
				<ul class="nav navbar-nav navbar-left text-center menuSuperior">
					<div class="row">	
						<li>
							<a title="Nosotros" href="nosotros.php">Nosotros</a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
						<li class="nav-item">
							<a title="Blog" href="#">Blog</a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
						<li class="nav-item">
							<a title="Contacto" href="contacto.php">Contacto</a>
						</li>
					</div>
				</ul>
			</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">	
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-google-plus"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li>
								<a href="acciones/iniciarSesion.php">Iniciar sesión</a>
							</li>
							<li>
								|
							</li>
							<li>
								<a href="acciones/iniciarSesion.php">Registrarme</a>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="detalleProducto.php?imagen=<?php echo rand(1, 20);?>">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Tiendas" href="#">TIENDAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Pride" href="#">PRIDE</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Configurar PC" href="#">CONFIGURAR PC</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Powered by Asus" href="#">POWERED BY ASUS</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<li>
						<a title="Carrito" href="#">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="body-content outer-top-xs">
					<div class="container">
						<div class="row">
							<div class="col-md-3 sidebar">
								<div class="side-menu animate-dropdown outer-bottom-xs">
									<div class="bg-dark text-white" id="menuVertical">
										<i class="icon fa fa-align-justify fa-fw"></i> CATEGORÍAS
									</div>
									<div class="todasCategorias">
										<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ACCESORIOS</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">AUDIO</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">COMPUTADORAS</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">COMPONENTES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">CONSUMIBLES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ALMACENAMIENTO</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ELECTRÓNICA</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">IMPRESIÓN</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">SOFTWARE</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">MONITORES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">CELULARES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">REDES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">PRIDE</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">APPLE</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<br>
								<div class="row">
									<div class="col-md-12">
				                        <label>Producto seleccionado: </label>
				                        <img src="public/imagenes/producto<?php echo $imagen;?>.png">
				                    </div>
								</div>
								<form action="realizarCompra.php" method="POST">
									<div class="row">
										<div class="col-md-4 form-group">
				                                <label>Nombre:</label>
				                                <input type="text" name="nombre" placeholder="Ingrese su nombre" required class="form-control">
					                    </div>
					                    <div class="col-md-4 form-group">
					                                <label>Correo:</label>
					                                <input type="email" name="correo" placeholder="Correo electrónico: algo@dominio.com" required class="form-control">
					                    </div>
					                    <div class="col-md-4 form-group">
					                                <label>Teléfono:</label>
					                                <input type="number" name="telefono" placeholder="Ingrese su número telefónico" required class="form-control">
					                    </div>
									</div>
				   					<div class="row">
				   						<div class="col-md-12 form-group">
				                                <label>Mensaje:</label>
				                                <textarea name="comentarios" placeholder="Información de compra." required class="form-control"></textarea>
				                    	</div>
				   					</div>
				   					<div class="row">
				   						<div class="col-md-6">
				   							<input type="submit" name="enviar" value="Confirmar compra" class="btn-success form-control">	
				   						</div>
				   					</div>
				                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
							<div class="centered">
						<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>		
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="public/imagenes/logoFooter.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
		}
	 ?>

	 
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	<script src="public/js/bootstrap.js"></script>
 </body>
 </html>