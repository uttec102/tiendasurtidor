<script type="text/javascript">
    function mensajeCorrecto(){
        alert("Gracias, en breve un representante del sitio se pondrá en contacto con usted.");
        location.href="contacto.php";
    }
    function mensajeIncorrecto(){
        alert("Captcha incorrecto. Verifique su información");
        location.href="contacto.php";
    }
</script>
<?php
	include("acciones/conexion.php");
	$mysqli->real_query("select * from t_producto");
	$query = $mysqli -> store_result();
	$numeroRegistros = mysqli_affected_rows($mysqli);  
?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Contacto</title>
 	<meta charset="utf-8">
 	<link rel="stylesheet" type="text/css" href="public/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="public/style.css">
	<link rel="stylesheet" href="./public/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/css/bootstrap.css">
 </head>
 <body>
 	<?php 
 	session_start();
	if(isset($_REQUEST["nombre"])){		
		$captcha = $_SESSION["captcha"];
		$recibirCaptcha = $_REQUEST['codigo'];
		if($captcha == $recibirCaptcha){
			$destino = "tiendasurtidor@outlook.com";//su correo
			$asunto = utf8_decode("Se ha solicitado información del sitio web");
			$mensaje = utf8_decode("La información es: ") . PHP_EOL .
			"Nombre: " . $_REQUEST["nombre"] . PHP_EOL .
			utf8_decode(" Teléfono: ") . $_REQUEST["telefono"] . PHP_EOL .
			"Correo: " . $_REQUEST["correo"] . PHP_EOL .
			"Comentarios: " . utf8_decode($_REQUEST["comentarios"]);
			$remitente = $_REQUEST["correo"];
			mail($destino, $asunto, $mensaje);
			echo "<script>";
	        echo "mensajeCorrecto();";
	        echo "</script>";

	        $destinoRemitente = $remitente;
	        $asuntoRemitente = "Sus comentarios son valiosos para nosotros";
	        $mensajeRemitente = "Su mensaje fue: " . PHP_EOL .
	        "Mensaje: " . utf8_decode($_REQUEST["comentarios"]) . PHP_EOL .
	        "Gracias, sus comentarios nos ayudan a mejorar.";
	        $remitenteNuevo = "tiendasurtidor@outlook.com";
	        mail($remitente, $asuntoRemitente, $mensajeRemitente);
		}else{
			echo "<script>";
	        echo "mensajeIncorrecto();";
	        echo "</script>";
		}
	}else{
 	 ?>
 	<div class="bg-dark">
		<div class="container">
			<div class="row">
			<div class="col-md-4 text-center">
				<ul class="nav navbar-nav navbar-left text-center menuSuperior">
					<div class="row">	
						<li>
							<a title="Nosotros" href="nosotros.php">Nosotros</a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Blog" href="#">Blog</a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Contacto" href="contacto.php">Contacto</a>
						</li>
					</div>
				</ul>
			</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">	
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-google-plus"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<?php 
								if (isset ($_SESSION['usuario']) )
								{
									//si existe
									//echo $_SESSION['usuario'];
							?>
									<li>
										<a href="acciones/cerrarSesion.php"><i class="fa fa-sign-out"></i>Cerrar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/modificarContrasenia.php">Cambiar contraseña</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/verCompra.php">Mis compras</a>
									</li>
							<?php		
								}
								else
								{									
									//echo "NO hay";
								
							?>
									<li>
										<a href="acciones/iniciarSesion.php">Iniciar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/iniciarSesion.php">Registrarme</a>
									</li>
							<?php 
								}
							?>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="public/imagenes/logo.png">			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="detalleProducto.php?imagen=<?php echo rand(1, $numeroRegistros);?>">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Tiendas" href="#">TIENDAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Pride" href="#">PRIDE</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Configurar PC" href="#">CONFIGURAR PC</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Powered by Asus" href="#">POWERED BY ASUS</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<li>
						<a title="Carrito" href="#">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
							
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<div class="body-content outer-top-xs">
					<div class="container">
						<div class="row">							
							<div class="col-md-9 productos">
								<img src="./public/imagenes/map1.png" width="100%">
							</div>
							<div class="col-md-3 contact-info">
								<br>
								<div class="contact-title">
									<h4>Sucursal Matriz</h4>
								</div><!-- contact-title -->
								<div class="clearfix address">
									<span class="contact-i"><i class="fa fa-map-marker"></i></span>
									<span class="contact-span">Calle Jose María Castilleros 3415 <br> Col. Lomas de Polanco <br> Guadalajara Jalisco, México</span>
								</div><!-- address -->
								<div class="clearfix phone-no">
									<span class="contact-i"><i class="fa fa-mobile"></i></span>
									<span class="contact-span">01 33 15231303
									<br>
									01 33 15232823</span>
								</div><!-- phone -->
								<div class="clearfix phone-no">
									<span class="contact-i"><i class="fa fa-clock-o"></i></span>
									<span class="contact-span">Lunes a Viernes: 09:00 - 02:30 y 03:30 - 06:00, Sábados: 10:00am - 01:00pm</span>
								</div><!-- clock -->
								<div class="clearfix email">
									<span class="contact-i"><i class="fa fa-envelope"></i></span>
									<span class="contact-span">ventas@ddtech.mx</span>
								</div><!-- email -->
								<br>
								<img title="Sucursal Matriz" alt="Sucursal Matriz - DD Tech" class="img-responsive center-block" src="https://ddtech.mx/assets/img/lomas.jpg?1544210484">				
							</div>
						</div>
						<div class="row">							
							<div class="col-md-9 productos" >
								<img src="./public/imagenes/map2.png" width="100%">
							</div>
							<div class="col-md-3 contact-info">
								<br>
								<div class="contact-title">
									<h4>Zona Centro</h4>
								</div><!-- contact-title -->
								<div class="clearfix address">
									<span class="contact-i"><i class="fa fa-map-marker"></i></span>
									<span class="contact-span">Calle Santa Monica 286 <br> Guadalajara Jalisco, México</span>
								</div><!-- address -->
								<div class="clearfix phone-no">
									<span class="contact-i"><i class="fa fa-mobile"></i></span>
									<span class="contact-span"><br>01 33 19843103</span>
								</div><!-- phone -->
								<div class="clearfix phone-no">
									<span class="contact-i"><i class="fa fa-clock-o"></i></span>
									<span class="contact-span">Lunes a Viernes: 10:00am - 07:00pm, Sábados: 10:00am - 05:00pm</span>
								</div><!-- clock -->
								<div class="clearfix email">
									<span class="contact-i"><i class="fa fa-envelope"></i></span>
									<span class="contact-span">ventas@ddtech.mx</span>
								</div><!-- email -->
								<br>
								<img title="Sucursal Zona Centro" alt="Sucursal Zona Centro - DD Tech" class="img-responsive center-block" src="https://ddtech.mx/assets/img/monica.jpg?1543969484">				
							</div>
						</div>
						<div class="row">							
							<div class="col-md-9 productos" >								
								<form method="get" action="contacto.php" class="form-contacto">
									<h4>¡Contactanos!</h4>
									<div class="row">
										<div class="col-md-4 form-group">
											<label for="">Nombre</label>
											<input type="text" name="nombre" placeholder="Nombre" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}" required class="form-control">
										</div>
										<div class="col-md-4 form-group">
											<label>Correo</label>
											<input type="mail" name="correo" placeholder="Correo" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required class="form-control">
										</div>
										<div class="col-md-4 form-group">
											<label>Teléfono</label>
											<input type="text" name="telefono" placeholder="Teléfono" pattern="[0-9]{10}" required class="form-control">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label>Tus comentarios</label>
											<textarea name="comentarios" type="text" placeholder="Escribe aquí tus comentarios" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,399}" required class="form-control message">
											</textarea>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12 form-group">
											<img src="captcha.php" style="border-radius:4px;">
											<br><label>Ingrese Código</label>
											<input type="text" name="codigo" required class="form-control" pattern="[0-9]{6}" placeholder="Ingrese captcha">
										</div>		
									</div>
									<div class="row">
										<div class="col-md-12 button-contactanos">
											<button class="btn-upper btn btn-primary checkout-page-button sendmail" title="Enviar mensaje" type="submit">Enviar mensaje</button>
										</div>										
									</div>								
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
							<div class="centered">
						<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>		
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="public/imagenes/logoFooter.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	}
	 ?>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	<script src="public/js/bootstrap.js"></script>
 </body>
 </html>