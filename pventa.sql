-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2019 a las 21:21:49
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pventa`
--
CREATE DATABASE IF NOT EXISTS `pventa` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `pventa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_almacen`
--

CREATE TABLE `t_almacen` (
  `id_almacen` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `entrada` double NOT NULL,
  `salida` double NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_almacen`
--

INSERT INTO `t_almacen` (`id_almacen`, `id_producto`, `entrada`, `salida`, `fecha`) VALUES
(4, 1, 7, 9, '2019-11-05'),
(5, 1, 9, 5, '2019-11-05'),
(6, 1, 9, 5, '2019-11-05'),
(7, 1, 9, 5, '2019-11-05'),
(12, 1, 0, 0, '0000-00-00'),
(13, 1, 9, 5, '2019-11-05'),
(14, 1, 9, 7, '2019-11-05'),
(15, 1, 10, 8, '2019-12-01'),
(16, 1, 4, 5, '2019-12-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_categorias`
--

CREATE TABLE `t_categorias` (
  `id_categoria` int(11) NOT NULL,
  `categoria` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_categorias`
--

INSERT INTO `t_categorias` (`id_categoria`, `categoria`) VALUES
(11, 'Ram'),
(12, 'SSD'),
(17, 'Prueba2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_cliente`
--

CREATE TABLE `t_cliente` (
  `id_cliente` int(11) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `contacto` varchar(60) NOT NULL,
  `telefono1` varchar(20) NOT NULL,
  `correo1` varchar(60) NOT NULL,
  `telefono2` varchar(20) NOT NULL,
  `correo2` varchar(60) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_colonia` int(11) NOT NULL,
  `id_codpos` int(11) NOT NULL,
  `calle` varchar(40) NOT NULL,
  `Noext` varchar(5) NOT NULL,
  `Noint` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_codpos`
--

CREATE TABLE `t_codpos` (
  `id_codpos` int(11) NOT NULL,
  `codpos` varchar(5) NOT NULL,
  `id_colonia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_codpos`
--

INSERT INTO `t_codpos` (`id_codpos`, `codpos`, `id_colonia`) VALUES
(1, '55749', 1),
(2, '33604', 2),
(3, '36700', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_colonias`
--

CREATE TABLE `t_colonias` (
  `id_colonia` int(11) NOT NULL,
  `colonia` varchar(60) NOT NULL,
  `id_municipio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_colonias`
--

INSERT INTO `t_colonias` (`id_colonia`, `colonia`, `id_municipio`) VALUES
(1, 'Sierra Hermosa', 1),
(2, '5 de mayo', 2),
(3, 'Guadalupana', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_compras`
--

CREATE TABLE `t_compras` (
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `cantidad` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_compras`
--

INSERT INTO `t_compras` (`id_compra`, `id_producto`, `fecha`, `id_proveedor`, `cantidad`) VALUES
(1, 1, '2019-11-06', 1, 15),
(2, 2, '2019-11-07', 2, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_estado`
--

CREATE TABLE `t_estado` (
  `id_estado` int(11) NOT NULL,
  `estado` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_estado`
--

INSERT INTO `t_estado` (`id_estado`, `estado`) VALUES
(1, 'Mexico'),
(2, 'Ciudad de Mexico'),
(3, 'Jalisco'),
(4, 'Nuevo Leon'),
(5, 'Guanajuato');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_imagenes`
--

CREATE TABLE `t_imagenes` (
  `id_imagenes` int(11) NOT NULL,
  `imagen` varchar(60) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `tipo` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_imagenes`
--

INSERT INTO `t_imagenes` (`id_imagenes`, `imagen`, `id_producto`, `tipo`, `status`) VALUES
(9, 'IMG_20181126_184443.jpg', 1, 1, 1),
(10, 'IMG_20181126_192621.jpg', 1, 0, 1),
(11, 'IMG_20181126_184515.jpg', 1, 1, 1),
(12, '', 1, 1, 0),
(17, 'IMG_20181126_184443.jpg', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_municipio`
--

CREATE TABLE `t_municipio` (
  `id_municipio` int(11) NOT NULL,
  `municipio` varchar(60) NOT NULL,
  `id_estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_municipio`
--

INSERT INTO `t_municipio` (`id_municipio`, `municipio`, `id_estado`) VALUES
(1, 'Tecamac', 1),
(2, 'Apodaca', 4),
(3, 'Salamanca', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_pedido`
--

CREATE TABLE `t_pedido` (
  `id_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `id_venta` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_producto`
--

CREATE TABLE `t_producto` (
  `id_producto` int(11) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `precom` double NOT NULL,
  `preven` double NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(30) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_producto`
--

INSERT INTO `t_producto` (`id_producto`, `producto`, `precom`, `preven`, `descripcion`, `imagen`, `id_categoria`) VALUES
(1, 'H500', 1000, 750, 'Descripcion nueva', 'IMG_20181126_184443.jpg', 11),
(2, 'ADATA', 1200, 1000, 'Disco duro', 'IMG_20181126_184443.jpg', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_proveedor`
--

CREATE TABLE `t_proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `proveedor` varchar(255) NOT NULL,
  `contacto` varchar(60) NOT NULL,
  `telefono1` varchar(20) NOT NULL,
  `correo1` varchar(60) NOT NULL,
  `telefono2` varchar(20) NOT NULL,
  `correo2` varchar(60) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_colonia` int(11) NOT NULL,
  `id_codpos` int(11) NOT NULL,
  `calle` varchar(40) NOT NULL,
  `Noext` varchar(5) NOT NULL,
  `Noint` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_proveedor`
--

INSERT INTO `t_proveedor` (`id_proveedor`, `proveedor`, `contacto`, `telefono1`, `correo1`, `telefono2`, `correo2`, `id_estado`, `id_municipio`, `id_colonia`, `id_codpos`, `calle`, `Noext`, `Noint`) VALUES
(1, 'ALFAWEB', '', '', '', '', '', 0, 0, 0, 0, '', '', ''),
(2, 'DDTECH', 'Julio Martinez', '551144778899', 'hola@hotmail.com', '553322669988', 'hello@hotmail.com', 1, 1, 1, 1, 'Sierra Hermosa', '2A', '4B'),
(3, 'Intel', 'Marco Torres Montes', '557788449966', 'juant@gmail.com', '557788449966', 'juanm@hotmail.com', 1, 1, 1, 1, 'Calle francisco I. Madero No58', '2B', '4F'),
(4, 'COFECE', 'Roberto Carlos Madrigal', '557788449966', 'robertoc@gmail.co', '551122334499', 'robertocm@outlook.co', 1, 1, 1, 1, 'Calle francisco I. Madero No58', '2A', '4B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_usuarios`
--

CREATE TABLE `t_usuarios` (
  `id_usuario` int(11) NOT NULL,
  `correo` varchar(60) NOT NULL,
  `contrapass` varchar(255) NOT NULL,
  `nevel` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `t_usuarios`
--

INSERT INTO `t_usuarios` (`id_usuario`, `correo`, `contrapass`, `nevel`, `status`) VALUES
(1, 'julio@hotmail.com', '$2y$10$5qTE6EyLV8aShh9TqDbuxujoGQiCsKeAclmcpIQuy8dX9EAiGInKO', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_ventas`
--

CREATE TABLE `t_ventas` (
  `id_venta` int(11) NOT NULL,
  `facrem` tinyint(4) NOT NULL,
  `monto` double NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `pdfventa` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `t_almacen`
--
ALTER TABLE `t_almacen`
  ADD PRIMARY KEY (`id_almacen`);

--
-- Indices de la tabla `t_categorias`
--
ALTER TABLE `t_categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `t_cliente`
--
ALTER TABLE `t_cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `t_codpos`
--
ALTER TABLE `t_codpos`
  ADD PRIMARY KEY (`id_codpos`);

--
-- Indices de la tabla `t_colonias`
--
ALTER TABLE `t_colonias`
  ADD PRIMARY KEY (`id_colonia`);

--
-- Indices de la tabla `t_compras`
--
ALTER TABLE `t_compras`
  ADD PRIMARY KEY (`id_compra`);

--
-- Indices de la tabla `t_estado`
--
ALTER TABLE `t_estado`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `t_imagenes`
--
ALTER TABLE `t_imagenes`
  ADD PRIMARY KEY (`id_imagenes`);

--
-- Indices de la tabla `t_municipio`
--
ALTER TABLE `t_municipio`
  ADD PRIMARY KEY (`id_municipio`);

--
-- Indices de la tabla `t_pedido`
--
ALTER TABLE `t_pedido`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indices de la tabla `t_producto`
--
ALTER TABLE `t_producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `t_proveedor`
--
ALTER TABLE `t_proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `t_usuarios`
--
ALTER TABLE `t_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `t_ventas`
--
ALTER TABLE `t_ventas`
  ADD PRIMARY KEY (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `t_almacen`
--
ALTER TABLE `t_almacen`
  MODIFY `id_almacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `t_categorias`
--
ALTER TABLE `t_categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `t_cliente`
--
ALTER TABLE `t_cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `t_codpos`
--
ALTER TABLE `t_codpos`
  MODIFY `id_codpos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `t_colonias`
--
ALTER TABLE `t_colonias`
  MODIFY `id_colonia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `t_compras`
--
ALTER TABLE `t_compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `t_estado`
--
ALTER TABLE `t_estado`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `t_imagenes`
--
ALTER TABLE `t_imagenes`
  MODIFY `id_imagenes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `t_municipio`
--
ALTER TABLE `t_municipio`
  MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `t_pedido`
--
ALTER TABLE `t_pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `t_producto`
--
ALTER TABLE `t_producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `t_proveedor`
--
ALTER TABLE `t_proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `t_usuarios`
--
ALTER TABLE `t_usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `t_ventas`
--
ALTER TABLE `t_ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
