<?php
	include("acciones/conexion.php");
	$mysqli->real_query("select * from t_producto");
	$query = $mysqli -> store_result();
	$numeroRegistros = mysqli_affected_rows($mysqli); 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="public/bootstrap.css">-->
	<script src="public/jquery.min.js"></script>
	<script src="public/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="public/style.css">
	<!--<script type="text/javascript" src="public/bootstrap.js"></script>-->
	<link rel="stylesheet" href="./public/font-awesome-4.7.0/css/font-awesome.min.css">	
	<!--<link rel="stylesheet" href="./public/ddtech.min.css">-->
	<!--<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet" type="text/css">-->

	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>



	<link rel="stylesheet" href="public/css/bootstrap.css">
</head>
<body>
	<div class="bg-dark barra-informativa">
		<div class="container">
			<div class="row">
			<div class="col-md-4 text-center">
				<ul class="nav navbar-nav navbar-left text-center menuSuperior">
					<div class="row">	
						<li>
							<a title="Nosotros" href="nosotros.php">Nosotros</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Blog" href="#">Blog</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Contacto" href="contacto.php">Contacto</a>
						</li>
					</div>
				</ul>
			</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-phone"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<?php 
								session_start();
								if (isset ($_SESSION['usuario']) )
								{									
									//si existe
									//echo $_SESSION['usuario'];
							?>
									<li>
										<a href="acciones/cerrarSesion.php"><i class="fa fa-sign-out"></i>Cerrar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/modificarContrasenia.php">Cambiar contraseña</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/verCompra.php">Mis compras</a>
									</li>
							<?php		
								}
								else
								{									
									//echo "NO hay";
								
							?>
									<li>
										<a href="acciones/iniciarSesion.php">Iniciar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/iniciarSesion.php">Registrarme</a>
									</li>
							<?php 
								}
							?>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="detalleProducto.php?imagen=<?php echo rand(1, $numeroRegistros);?>">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Tiendas" href="#">TIENDAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Pride" href="#">PRIDE</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Configurar PC" href="#">CONFIGURAR PC</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Powered by Asus" href="#">POWERED BY ASUS</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<li>
						<a title="Carrito" href="shopping-cart.php">
							<span id="cart-quantity">
								0
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="body-content outer-top-xs">
					<div class="container">
						<div class="row">
							<div class="col-md-3 sidebar">
								<div class="side-menu animate-dropdown outer-bottom-xs">
									<div class="bg-dark text-white" id="menuVertical">
										<i class="icon fa fa-align-justify fa-fw"></i> CATEGORÍAS
									</div>
									<div class="todasCategorias">
										<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">										
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ACCESORIOS</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">AUDIO</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">COMPUTADORAS</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">COMPONENTES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">CONSUMIBLES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ALMACENAMIENTO</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ELECTRÓNICA</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">IMPRESIÓN</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">SOFTWARE</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">MONITORES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">CELULARES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">REDES</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">PRIDE</a>
											<a class="dropdown-toggle nav-link" id="categorias" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">APPLE</a>
										</div>
									</div>
								</div>
								<div class="ofertas">
									<?php 
										$mysqli->real_query("select * from t_imagenes WHERE tipo = 2");
										$query = $mysqli->store_result();										
										while($row = $query->fetch_assoc()){ 
									 ?>
											<a href="detalleProducto.php?imagen=<?php echo $row['id_producto'] ?>"><img src="images/<?php echo $row['imagen'];?>"></a>
									<?php } ?>
									<!--<img src="public/imagenes/oferta2.jpg">
									<img src="public/imagenes/oferta3.jpg">
									<img src="public/imagenes/oferta4.jpg">
									<img src="public/imagenes/oferta5.jpg">-->
									<div id="no">
										<img src="public/imagenes/oferta6.jpg">
										<img src="public/imagenes/seguro.jpg">
										<img src="public/imagenes/msi.jpg">
										<img src="public/imagenes/teamgroup.jpg">
										<img src="public/imagenes/toshiba.jpg">
									</div>
								</div>
							</div>
							<div class="col-md-9 productos">


							<div class="row">                    
								<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
									</ol>
									<div class="carousel-inner">	
										<?php 
											$mysqli->real_query("select * from t_imagenes WHERE tipo = 1");
											$query = $mysqli->store_result();										
											$row = $query->fetch_assoc()
										?>
										<div class="carousel-item active">
											<a href="detalleProducto.php?imagen=<?php echo $row['id_producto'] ?>"><img src="images/<?php echo $row['imagen'];?>" class="d-block w-100" alt="..." style="height:485px;"></a>
										</div>
										<?php 
										$mysqli->real_query("select * from t_imagenes WHERE tipo = 1");
										$query = $mysqli->store_result();										
										while($row = $query->fetch_assoc()){ 
										?>
										<div class="carousel-item">
											<a href="detalleProducto.php?imagen=<?php echo $row['id_producto'] ?>"><img src="images/<?php echo $row['imagen'];?>" class="d-block w-100" alt="..." style="height:485px;"></a>
										</div>
										<?php } ?>
									</div>
									<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>

								<!--<div class="col-md-12 carrusel">
									<img src="public/img/banner.jpeg">
								</div>   -->         
							</div>


								<!--<img src="public/imagenes/carrusel.png" width="100%">-->
								<div class="todosProductos">
									<h3 style="color:#808080; font: bold 14px 'Arial';">
										PRODUCTOS DESTACADOS
									</h3><hr>
									<div class="row">
										<?php 
											include 'acciones/conexion.php';
											$mysqli->real_query("select * from t_producto");
											$query = $mysqli->store_result();										
											while($row = $query->fetch_assoc()){          												
												
										?>
												<div class="col-sm-12 col-md-3">																																				
													<div class="row">
														<div class="col-md-12">
															<center>
																<img src="images/<?php echo $row['imagen'];?>" alt="" srcset="" width="95%" height="120px">
															</center>														
														</div>														
													</div>													
													<div class="row">
														<div class="col-sm-10 col-md-12">
															<br>
															<div class="text-left">
																<h3 class="name-producto">
																	<a class="link-name-producto">
																		<b><?php echo $row['producto']?></b>
																	</a>																	
																</h3>																																
															</div>	
															<div class="text-left">
																<h3 class="price-producto">																	
																	<span class="price-span">
																		$<?php echo $row['precom']?>.00
																	</span>																	
																</h3>
															</div>
															<div class="text-left">
																<button href="" class="btn btn-secondary btn-block" onclick="agregarCarrito(<?php echo $row['id_producto']?>)">																	
																	<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Agregar																					
																</button>
															</div>	
															<div class="text-left">
																<center>																
																	<a href="detalleProducto.php?imagen=<?php echo $row['id_producto']?>" class="btn mas-detalles">																	
																		<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Detalles																					
																	</a>
																</center>
															</div>														
														</div>														
													</div>
													<!--<a href="detalleProducto.php?imagen=<?php //echo $i;?>"><img src="public/imagenes/producto<?//php echo $i;?>.png" width="80%"></a>-->												
												</div>											
										<?php
											}
										 ?>
									</div>
									<div class="ultimos">
										<h3 style="color:#808080; font: bold 14px 'Arial';">ÚLTIMOS PRODUCTOS</h3>
										<button><</button>
										<button>></button><hr>
									</div>
									<div class="row">
										<?php 
											//for($i = 17; $i <= 20; $i++){
											include 'acciones/conexion.php';
											$mysqli->real_query("select * from t_producto order by id_producto DESC limit 4");
											$query = $mysqli->store_result();										
											while($row = $query->fetch_assoc()){ 
										?>

												<div class="col-sm-12 col-md-3">																																				
													<div class="row">
														<div class="col-md-12">
															<center>
																<img src="images/<?php echo $row['imagen'];?>" alt="" srcset="" width="95%" height="120px">
															</center>														
														</div>														
													</div>													
													<div class="row">
														<div class="col-sm-10 col-md-12">
															<br>
															<div class="text-left">
																<h3 class="name-producto">
																	<a class="link-name-producto">
																		<b><?php echo $row['producto']?></b>
																	</a>																	
																</h3>																																
															</div>	
															<div class="text-left">
																<h3 class="price-producto">																	
																	<span class="price-span">
																		$<?php echo $row['precom']?>.00
																	</span>																	
																</h3>
															</div>
															<div class="text-left">
																<button href="" class="btn btn-secondary btn-block" onclick="agregarCarrito(<?php echo $row['id_producto']?>)">																	
																	<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Agregar																					
																</button>
															</div>	
															<div class="text-left">
																<center>																
																	<a href="detalleProducto.php?imagen=<?php echo $row['id_producto']?>" class="btn mas-detalles">																	
																		<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Detalles																					
																	</a>
																</center>
															</div>													
														</div>														
													</div>
													<!--<a href="detalleProducto.php?imagen=<?php //echo $i;?>"><img src="public/imagenes/producto<?//php echo $i;?>.png" width="80%"></a>-->												
												</div>		

												<!--<div class="col-sm-12 col-md-3">
														<center>
															<a href="detalleProducto.php?imagen=<?php echo $i;?>"><img src="public/imagenes/producto<?php echo $i; ?>.png" width="80%"></a>
														</center>											
												</div>-->
										<?php
											}
										 ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
							<div class="centered">
						<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>	
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="public/imagenes/logo-Footer.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script type="text/javascript" src="public/js/jquery-3.0.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	<script src="public/js/bootstrap.js"></script>
	<script src="public/sweetalert.js"></script>
	<script src="public/js/carrito.js"></script>	
</body>
</html>