<!DOCTYPE html>
<html>
<head>
	<title>Carrito</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" type="text/css" href="public/bootstrap.css">-->
	<script src="public/jquery.min.js"></script>
	<script src="public/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="public/style.css">
	<!--<script type="text/javascript" src="public/bootstrap.js"></script>-->
	<link rel="stylesheet" href="./public/font-awesome-4.7.0/css/font-awesome.min.css">	
	<!--<link rel="stylesheet" href="./public/ddtech.min.css">-->
	<!--<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet" type="text/css">-->

	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>



	<link rel="stylesheet" href="public/css/bootstrap.css">
</head>
<body>
	<div class="bg-dark barra-informativa">
		<div class="container">
			<div class="row">
			<div class="col-md-4 text-center">
				<ul class="nav navbar-nav navbar-left text-center menuSuperior">
					<div class="row">	
						<li>
							<a title="Nosotros" href="nosotros.php">Nosotros</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Blog" href="#">Blog</i></a>
						</li>
						<li class="menu-sep hidden-xs">
							|
						</li>
					
						<li class="nav-item">
							<a title="Contacto" href="contacto.php">Contacto</a>
						</li>
					</div>
				</ul>
			</div>
				<div class="col-md-5">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<li class="nav-item">
								<a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a title="Youtube" href="#"><i class="fa fa-youtube-play"></i></a>
							</li>
							<li class="nav-item">
								<a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
							</li>
							<li class="nav-item">
								<a title="Google" href="#"><i class="fa fa-phone"></i></a>
							</li>
							<li>
								|
							</li>
							<li class="nav-item">
								<a title="Contacto" href="#"><i class="fa fa-phone"></i>(33) 15 23 28 23</a>
							</li>
							<li>
								|
							</li>
						</div>
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="nav navbar-nav navbar-left text-center menuSuperior">
						<div class="row">
							<?php 
                                session_start();
                                if(!isset($_SESSION['cart'])){
                                    header("Location:index.php");
                                }
								if (isset ($_SESSION['usuario']) )
								{									
							?>
									<li>
										<a href="acciones/cerrarSesion.php"><i class="fa fa-sign-out"></i>Cerrar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/modificarContrasenia.php">Cambiar contraseña</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/verCompra.php">Mis compras</a>
									</li>
							<?php		
								}
								else
								{																										
							?>
									<li>
										<a href="acciones/iniciarSesion.php">Iniciar sesión</a>
									</li>
									<li>
										|
									</li>
									<li>
										<a href="acciones/iniciarSesion.php">Registrarme</a>
									</li>
							<?php 
								}
							?>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1765be;">
		<div class="container">
			<img src="public/imagenes/logo.png">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse barra-navegacional" id="navbarSupportedContent">
				<ul class="nav navbar-nav navbar-left text-center">
					<li class="nav-item">
						<a title="Inicio" href="index.php"><i class="fa fa-home"></i></a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Productos" href="detalleProducto.php?imagen=<?php echo rand(1, 20);?>">PRODUCTOS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Tiendas" href="#">TIENDAS</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Pride" href="#">PRIDE</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Configurar PC" href="#">CONFIGURAR PC</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li class="nav-item">
						<a title="Powered by Asus" href="#">POWERED BY ASUS</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right text-center">
					<li class="search-container">
						<form class="navbar-form navbar-right">
							<div class="input-group mb-3 info">
								<input type="text" class="form-control info" id="buscar" aria-label="Buscar" aria-describedby="button-addon2">
								<!--<div class="input-group-append">
									<button class="btn btn-outline btn-sm" type="button" id="button-addon2" id="buscarButton">Buscar</button>
								</div>-->
								<input type="submit" name="buscar" id="buscarButton" value="Buscar">
							</div>
						</form>
					</li>
					<li>
						<a title="Carrito" href="shopping-cart.php">
							<span id="cart-quantity">
								<?php
									echo count($_SESSION['cart']);
								?>								
							</span>
							<i class="fa fa-shopping-cart"></i>
						</a>
					</li>
					<li class="menu-sep hidden-xs">
						|
					</li>
					<li>
						<a title="Comprar" href="#">Comprar</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="body-content outer-top-xs">
					<div class="container">
						<div class="row">							
							<div class="col-md-12 productos">
                                <div class="row flex-row-reverse">
                                    <div class="col-md-3 col-sm-12">
                                        <button class="btn btn-danger btn-block" onclick="vaciarCarrito()"><i class="fa fa-trash-o" aria-hidden="true"></i> Vaciar carrito</button>
                                        <br>                                        
                                    </div>
                                </div>
                                <div class="row">  
                                    <div class="table-responsive">
                                        <table class="table table-cart table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Producto</th>
                                                    <th scope="col" width="240px">Descripción</th>
                                                    <th scope="col">Cantidad</th>
                                                    <th scope="col">Precio</th>
                                                    <th scope="col">Precio total</th>
                                                    <th scope="col">Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
												<?php 
													//print_r($_SESSION['cart']);
													include 'acciones/conexion.php';
													$carrito = $_SESSION['cart'];	
													$total;
													$contador=0;
													foreach ($carrito as $c){
														//print_r($c['id_pedido']);
														//$id_pedido = $carrito[$i]['id_pedido'];
														$mysqli->real_query("select id_pedido,id_producto,cantidad from t_pedido where id_pedido = ".$c['id_pedido']);
														$query = $mysqli->store_result();
														while($row = $query->fetch_assoc()){   
															//Producto	
															$mysqli->real_query("select * from t_producto where id_producto = ".$row['id_producto']);
															$query2 = $mysqli->store_result();       
															while($row2 = $query2->fetch_assoc()){
																//$total += ($row2['precom'] * $row['cantidad']);
																if($contador>0){
																	$total = $total + ($row2['precom'] * $row['cantidad']);
																}else{
																	$total = ($row2['precom'] * $row['cantidad']);
																}																																		
																$contador++;
												?>
																<tr id="productoCarrito<?php echo $row['id_pedido'];?>">
																	<td><img src="./images/<?php echo $row2['imagen'];?>" width="80px;"></td>
																	<td class="text-left"><?php echo $row2['descripcion']?></td>
																	<td><input type="number" onchange="updateCarrito(this.value,<?php echo $row['id_pedido'];?>)" class="cantidadPedido text-center" value="<?php echo $row['cantidad'];?>"></td>
																	<td>$<?php echo $row2['precom']?></td>
																	<td>$<?php echo ($row2['precom'] * $row['cantidad'])?></td>
																	<td><center><i class="fa fa-trash-o" aria-hidden="true" onclick="eliminarPedido(<?php echo $row['id_pedido'];?>)"></i></center></td>                                            
																</tr>
												<?php 
															}
														}
														//echo $total;
													}
													$total2 = $total + 230;
												?>                                            
                                            </tbody>
                                        </table>
                                    </div>                                                             
                                </div>
                                <div class="row">                                   
                                    <div class="col-md-8 col-sm-12">
                                        <table class="table table-bordered tablas-compras">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <span class="estimate-title">Tipo de entrega</span>
                                                        <p class="tablas-detalles">Todos los envíos se realizan en días hábiles.</p>
                                                    </th>
                                                </tr>
                                            </thead><!-- /thead -->
                                            <tbody>
                                                <tr>
                                                    <td id="send-method">
                                                        <div class="form-group tablas-detalles">
                                                            <label class="info-title control-label">
                                                                Tipo de entrega
                                                            </label>
                                                            <select id="select-send-method" class="form-control ddtech-form-control">
                                                                <option value="address">A domicilio</option>
                                                                <!--<option value="branch">En sucursal</option>-->
                                                            </select>
                                                        </div><!-- form-group -->
                                                        <div id="parcel" class="form-group tablas-detalles">
                                                            <label class="info-title control-label">
                                                                Envío
                                                            </label>
                                                            <select id="select-parcel" class="form-control ddtech-form-control">                                                                
                                                                <option value="13" >$130.00 (DHL)</option>                                                                
                                                            </select>
                                                        </div><!-- form-group -->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <table class="table table-bordered checkout-terms tablas-compras">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="cart-sub-total form-horizontal">
                                                            <div class="row">                                                                
                                                                <label class="col-sm-7 control-label text-center">Subtotal</label>
                                                                <label class="col-sm-5 control-label text-center" id="subtotal-price">$ <?php echo $total;?>.00</label>
                                                            </div><!-- form-group -->
                                                        </div>
                                                        <div class="cart-sub-total form-horizontal">
                                                            <div class="row">
                                                                <label class="col-sm-7 control-label text-center">Envío</label>
                                                                <label class="col-sm-5 control-label text-center" id="shipping-price">$130.00</label>
                                                            </div><!-- form-group -->
                                                        </div>
                                                        <div id="shipping-insurance-price-div" class="cart-sub-total form-horizontal">
                                                            <div class="row">
                                                                <label class="col-sm-7 control-label text-center">Seguro de envío</label>
                                                                <label class="col-sm-5 control-label text-center" id="shipping-insurance-price">$100.00</label>
                                                            </div><!-- form-group -->
                                                        </div>
                                                        <div class="cart-grand-total form-horizontal">
                                                            <div class="row div-total">
                                                                <label class="col-sm-7 control-label text-center" style="">Total <br>(IVA Incluido)</label>
                                                                <label class="col-sm-5 control-label text-center" id="total-price" id="total-price">$ <?php echo $total2;?>.00</label>
                                                            </div><!-- form-group -->
                                                        </div>
                                                        <span class="payment-status"></span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>
                                                        <center>
                                                            <a title="Continuar con el pago" class="btn btn-secondary cart-checkout-btn" id="continue" href="./checkout.php">Continuar con el pago</a>
                                                        </center>                                                        
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="col-md-12">
			<div class="row sinpadding">
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
							<div class="centered">
						<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg2.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
						<img src="public/bg1.png" width="100%">
						<div class="centered">
							<h3 style="color:#fff; font: bold 14px 'Arial';">Configurador</h3>
							<p style="font: 10px 'Arial';">
								¿Buscas la computadora ideal para ti?, Encuentra tu computadora perfecta respondiendo estas simples preguntas. Tendrás lo que necesitas al instante.
							</p>
							<p>
								<hr class="sitio">
								<a title="Ir al sitio" href="#" style="color: #fff; text-decoration: none; font: 10px 'Arial';">IR AL SITIO</a>
							</p>
						</div>
					</div>						
				</div>
			</div>	
		</div>
		<div class="informacion">
			<div class="col-md-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Configurar"><li>Configurar</li></a>
									<a href="#" title="Nosotros"><li>Nosotros</li></a>
									<a href="#" title="Blog"><li>Blog</li></a>
									<a href="#" title="Contacto"><li>Contacto</li></a>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<a href="#" title="Pride"><li>Pride</li></a>
									<a href="#" title="Preguntas frecuentes"><li>Preguntas frecuentes</li></a>
									<a href="#" title="Aviso legal"><li>Aviso legal</li></a>
									<a href="#" title="Políticas de envío"><li>Políticas de envío</li></a>
									<a href="#" title="Políticas de garantías"><li>Políticas de garantías</li></a>
								</ul>
							</div>
							<div class="col-md-4">
								<center><img src="public/imagenes/logo-Footer.png"></center>
								<p style="text-align: center">
									DD Tech &copy; 2019<br>
									Desarrollado por <b>Sector Web</b>
								</p>
							</div>
							<div class="col-md-4">
								<p>Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México</p>
								<p>(33) 15 23 28 23</p>
								<p>ventas@ddtech.mx</p>
								<button type="button" class="btn btn-primary btn-sm">Me gusta</button>
								<button type="button" class="btn btn-primary btn-sm">Compartir</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="empresas">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<img src="public/imagenes/logosPagos.png" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script type="text/javascript" src="public/js/jquery-3.0.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 	<script src="public/js/bootstrap.js"></script>
	<script src="public/sweetalert.js"></script>
	<script src="public/js/carrito.js"></script>	
</body>
</html>